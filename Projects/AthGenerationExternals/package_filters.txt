# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthGenerationExternals.
#
+ External/CLHEP
+ External/COOL
+ External/CORAL
+ External/GPerfTools
+ External/Gaudi
+ External/Gdb
+ External/GeoModel
+ External/GoogleTest
+ External/MKL
+ External/PyModules
+ External/dSFMT
+ External/flake8_atlas
+ External/prmon
+ External/yampl
- .*
