# ATLAS Specific HIP Build Configuration

This is a CMake configuration for using HIP as a "first-class" language in
CMake. It is meant to be used like:

```cmake
set( AtlasHIP_DIR "${CMAKE_SOURCE_DIR}/atlasexternals/Build/AtlasHIP"
   CACHE PATH "Location of AtlasHIP" )
find_package( AtlasHIP )
enable_language( HIP )
```
