# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Module finding HighFive in the LCG release. Defines:
#  - HIGHFIVE_FOUND
#  - HighFive (imported target)
#  - HIGHFIVE_INCLUDE_DIRS
#
# Can be steered by highfive_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Wrap the external's own CMake code.
lcg_wrap_find_module( HighFive
   NO_LIBRARY_DIRS
   IMPORTED_TARGETS HighFive )

# Set up the RPM dependency:
lcg_need_rpm( highfive )
