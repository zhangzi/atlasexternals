# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Module finding Superchic in the LCG release. Defines:
#  - SUPERCHIC_FOUND
#  - SUPERCHIC_DATA_PATH
#  - SUPERCHIC_superchic_EXECUTABLE
#
# Can be steered by SUPERCHIC_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME Superchic
   BINARY_NAMES superchic
   BINARY_SUFFIXES bin bin32 bin64)

find_path( SUPERCHIC_DATA_PATH
   NAMES "SplinesWithVariableKnots.dat" "ident_card.dat"
   PATHS "${SUPERCHIC_LCGROOT}"
   PATH_SUFFIXES "share/SuperChic" "Cards" )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( Superchic DEFAULT_MSG SUPERCHIC_superchic_EXECUTABLE )
mark_as_advanced( SUPERCHIC_FOUND SUPERCHIC_superchic_EXECUTABLE SUPERCHIC_DATA_PATH)

# Set up the RPM dependency:
lcg_need_rpm( superchic )

# Set the SUPERCHIC environment variable:
if( SUPERCHIC_DATA_PATH )
   set( SUPERCHIC_ENVIRONMENT FORCESET SUPERCHIC_DATA_PATH ${SUPERCHIC_DATA_PATH} )
else()
   message( STATUS
      "Superchic data cards were not found, SUPERCHIC_DATA_PATH is not set." )
endif()
