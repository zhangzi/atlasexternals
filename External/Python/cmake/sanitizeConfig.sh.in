#!/bin/bash
#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# Script used to sanitize the python installation produced by the build,
# so that it would be relocatable.
#
# Note that all sed commands use ":" as the separator in the sed expressions,
# so that path names (which have plenty of "/" in them) would not have to be
# massaged before giving them to the command.
#

# Fail on errors:
set -e

# Loop over the executable scripts that need to be massaged:
for script in @_buildDir@/bin/python3.11-config \
              @_buildDir@/bin/2to3-3.11 \
              @_buildDir@/bin/idle3.11 \
              @_buildDir@/bin/pydoc3.11; do

    # Create a sanitized version of the script, by just replacing
    # its first line with a relocatable expression:
    sed -i -e '1s:.*:#!/usr/bin/env python3:' ${script}
done

# Create a sanitized version of _sysconfigdata.py, using sed:
sed -i -e '1i\
import os; installdir=os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))' \
    @_buildDir@/lib/python3.11/_sysconfigdata*.py
sed -i -e "s:'@_buildDir@:installdir + ':g" \
    @_buildDir@/lib/python3.11/_sysconfigdata*.py
sed -i -e "s:installdir + '\(.*\)'$:installdir + '\1' +:g" \
    @_buildDir@/lib/python3.11/_sysconfigdata*.py
