# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building Python 3 if it's not available on the
# build machine.
#

# The name of the package:
atlas_subdir( Python )

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_PYTHON )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Python as part of this project" )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 OLD )
endif()

# Declare where to get libffi from.
set( ATLAS_LIBFFI_SOURCE
   "URL;https://cern.ch/lcgpackages/tarFiles/sources/libffi-3.4.2.tar.gz;URL_MD5;294b921e6cf9ab0fbaea4b639f8fdbe8"
   CACHE STRING "The source for libffi" )
mark_as_advanced( ATLAS_LIBFFI_SOURCE )

# Decide whether / how to patch the libffi sources.
set( ATLAS_LIBFFI_PATCH "" CACHE STRING "Patch command for libffi" )
set( ATLAS_LIBFFI_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of libffi (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_LIBFFI_PATCH ATLAS_LIBFFI_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the libffi build results.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/LibffiBuild" )

# Files/directories generated by the build.
if( NOT "${CMAKE_INSTALL_INCLUDEDIR}" STREQUAL "" )
   set( LIBFFI_INCLUDE_DIR "${_buildDir}/${CMAKE_INSTALL_INCLUDEDIR}" )
else()
   set( LIBFFI_INCLUDE_DIR "${_buildDir}/include" )
endif()
file( MAKE_DIRECTORY "${LIBFFI_INCLUDE_DIR}" )
if( NOT "${CMAKE_INSTALL_LIBDIR}" STREQUAL "" )
   set( LIBFFI_LIBRARY_DIR "${_buildDir}/${CMAKE_INSTALL_LIBDIR}" )
else()
   set( LIBFFI_LIBRARY_DIR "${_buildDir}/lib" )
endif()
set( LIBFFI_LIBRARY
   "${LIBFFI_LIBRARY_DIR}/${CMAKE_SHARED_LIBRARY_PREFIX}ffi${CMAKE_SHARED_LIBRARY_SUFFIX}" )

# Set up the build of libffi.
ExternalProject_Add( libffi
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_LIBFFI_SOURCE}
   ${ATLAS_LIBFFI_PATCH}
   CONFIGURE_COMMAND "<SOURCE_DIR>/configure" --prefix=${_buildDir}
   --includedir=${LIBFFI_INCLUDE_DIR} --libdir=${LIBFFI_LIBRARY_DIR}
   --disable-multi-os-directory
   BUILD_BYPRODUCTS "${LIBFFI_LIBRARY}" )
ExternalProject_Add_Step( libffi forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_LIBFFI_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( libffi purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for libffi"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( libffi buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing libffi into the build area"
   DEPENDEES install )
add_dependencies( Package_Python libffi )

# Set up imported targets that could be used by other packages in their builds.
add_library( libffi::ffi SHARED IMPORTED GLOBAL )
set_target_properties( libffi::ffi PROPERTIES
   INTERFACE_INCLUDE_DIRECTORIES "${LIBFFI_INCLUDE_DIR}"
   IMPORTED_LOCATION "${LIBFFI_LIBRARY}" )

# Install libffi:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Declare where to get Python from.
set( ATLAS_PYTHON_SOURCE
   "URL;https://cern.ch/lcgpackages/tarFiles/sources/Python-3.11.9.tgz;URL_MD5;bfd4d3bfeac4216ce35d7a503bf02d5c"
   CACHE STRING "The source for Python" )
mark_as_advanced( ATLAS_PYTHON_SOURCE )

# Decide whether / how to patch the Python sources.
set( ATLAS_PYTHON_PATCH "" CACHE STRING "Patch command for Python" )
set( ATLAS_PYTHON_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of Python (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_PYTHON_PATCH ATLAS_PYTHON_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PythonBuild" )

# Extra environment options for the configuration.
set( _compilerSetup )
set( _cflags )
set( _ldflags )
if( APPLE )
   list( APPEND _ldflags -Wl,-rpath,'@loader_path/../lib' )
else()
   list( APPEND _compilerSetup
      "export CC=${CMAKE_C_COMPILER}"
      "export CXX=${CMAKE_CXX_COMPILER}" )
   list( APPEND _ldflags -Wl,-rpath,'\\$\\$ORIGIN/../lib' )
endif()
list( APPEND _cflags -I${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/include )
list( APPEND _ldflags -L${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/lib
   -L${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/lib64 )
if( CMAKE_OSX_SYSROOT )
   list( APPEND _cflags -isysroot ${CMAKE_OSX_SYSROOT}
                        -I${CMAKE_OSX_SYSROOT}/usr/include )
   list( APPEND _ldflags -isysroot ${CMAKE_OSX_SYSROOT} )
endif()

# Extra configuration parameters for the build.
set( _extraArgs )

# Massage the options to make them usable in the configuration script.
string( REPLACE ";" "\n" _compilerSetup "${_compilerSetup}" )
string( REPLACE ";" " " _cflags "${_cflags}" )
string( REPLACE ";" " " _ldflags "${_ldflags}" )
string( REPLACE ";" " " _extraArgs "${_extraArgs}" )

# Create the scripts used in the build.
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeConfig.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh"
   @ONLY )
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh" @ONLY )

# Files/directories generated by the build.
set( Python_EXECUTABLE "${_buildDir}/bin/python3" )
set( Python_INCLUDE_DIR "${_buildDir}/include/python3.11" )
file( MAKE_DIRECTORY "${Python_INCLUDE_DIR}" )
set( Python_LIBRARY
   "${_buildDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}python3.11${CMAKE_SHARED_LIBRARY_SUFFIX}" )

# Set up the build of Python in the build directory:
ExternalProject_Add( Python
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_PYTHON_SOURCE}
   ${ATLAS_PYTHON_PATCH}
   CONFIGURE_COMMAND
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh"
   BUILD_BYPRODUCTS "${Python_LIBRARY}" )
ExternalProject_Add_Step( Python forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_PYTHON_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( Python purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Python"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Python buildinstall
   COMMAND "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh"
   COMMAND ${CMAKE_COMMAND} -E create_symlink python3 "${_buildDir}/bin/python"
   COMMAND ${CMAKE_COMMAND} -E create_symlink python3-config
   "${_buildDir}/bin/python-config"
   COMMAND ${CMAKE_COMMAND} -E create_symlink pydoc3 "${_buildDir}/bin/pydoc"
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing Python into the build area"
   DEPENDEES install )
add_dependencies( Package_Python Python )
add_dependencies( Python libffi SQLite )

# Install Python:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Set up imported targets that could be used by other packages in their builds.
add_executable( Python::Interpreter IMPORTED GLOBAL )
set_target_properties( Python::Interpreter PROPERTIES
   IMPORTED_LOCATION "${Python_EXECUTABLE}" )
add_library( Python::Python SHARED IMPORTED GLOBAL )
set_target_properties( Python::Python PROPERTIES
   INTERFACE_INCLUDE_DIRECTORIES "${Python_INCLUDE_DIR}"
   IMPORTED_LOCATION "${Python_LIBRARY}" )
