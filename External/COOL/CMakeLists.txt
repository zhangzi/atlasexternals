# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Package building COOL as part of the offline build procedure.
#

# The name of the package.
atlas_subdir( COOL )

# Check whether COOL is to be built in the current project.
if( NOT ATLAS_BUILD_COOL )
   return()
endif()

# Let the user know what's happening.
message( STATUS "Building COOL as part of this project" )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get COOL from.
set( ATLAS_COOL_SOURCE
   "URL;https://gitlab.cern.ch/lcgcool/cool/-/archive/COOL_3_3_18/cool-COOL_3_3_18.tar.gz;URL_MD5;fdfdd3a40814d42cadb23aae2a430179"
   CACHE STRING "The source for COOL" )
mark_as_advanced( ATLAS_COOL_SOURCE )

# Decide whether / how to patch the COOL sources.
set( ATLAS_COOL_PATCH "" CACHE STRING "Patch command for COOL" )
set( ATLAS_COOL_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of COOL (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_COOL_PATCH ATLAS_COOL_FORCEDOWNLOAD_MESSAGE )

# Arguments to give to the CMake configuration of COOL.
set( _cmakeArgs )
set( _cmakePrefixes )

# Set up where to get CORAL from.
if( ATLAS_BUILD_CORAL )
   list( APPEND _cmakePrefixes "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}" )
else()
   find_package( CORAL )
   list( APPEND _cmakePrefixes
      $<TARGET_PROPERTY:CORAL::CoralBase,INSTALL_PATH> )
endif()

# Set up where to get Python from.
if( ATLAS_BUILD_PYTHON )
   list( APPEND _cmakeArgs
      -DPYTHON_EXECUTABLE:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_BINDIR}/python${CMAKE_EXECUTABLE_SUFFIX}
      -DPYTHON_LIBRARY:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}Python${CMAKE_SHARED_LIBRARY_SUFFIX}
      -DPYTHON_INCLUDE_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_INCLUDEDIR}
      -DPython_config_version_twodigit:STRING=3.11
      -DLCG_python3:STRING=on )
else()
   find_package( Python COMPONENTS Interpreter Development )
   if( Python_FOUND )
      list( APPEND _cmakeArgs
         -DPYTHON_EXECUTABLE:FILEPATH=${Python_EXECUTABLE}
         -DPYTHON_LIBRARY:FILEPATH=${Python_LIBRARIES}
         -DPYTHON_INCLUDE_DIR:PATH=${Python_INCLUDE_DIRS}
         -DPython_config_version_twodigit:STRING=${Python_VERSION_MAJOR}.${Python_VERSION_MINOR} )
      if( ${Python_VERSION} VERSION_GREATER_EQUAL 3 )
         list( APPEND _cmakeArgs -DLCG_python3:STRING=on )
      endif()
   endif()
endif()

# Set up whete to get Boost from.
if( ATLAS_BUILD_BOOST )
   list( APPEND _cmakeArgs
      -DBOOST_ROOT:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   find_package( Boost CONFIG )
   if( Boost_FOUND )
      list( APPEND _cmakeArgs
         -DBOOST_ROOT:PATH=${Boost_DIR} )
   endif()
endif()

# Set up where to get CppUnit from.
find_package( CppUnit )
if( CPPUNIT_FOUND )
   list( APPEND _cmakeArgs
      -DCPPUNIT_INCLUDE_DIR:PATH=${CPPUNIT_INCLUDE_DIR}
      -DCPPUNIT_LIBRARY:FILEPATH=${CPPUNIT_cppunit_LIBRARY} )
endif()

# Set up where to get ROOT from.
if( ATLAS_BUILD_ROOT )
   list( APPEND _cmakePrefixes "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}" )
else()
   find_package( ROOT )
   if( ROOT_FOUND )
      list( APPEND _cmakePrefixes $<TARGET_PROPERTY:ROOT::Core,INSTALL_PATH> )
   endif()
endif()

# Set up where to get TBB from.
if( ATLAS_BUILD_TBB )
   list( APPEND _cmakeArgs
      -DTBB_LIBRARIES:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}tbb${CMAKE_SHARED_LIBRARY_SUFFIX} )
else()
   find_package( TBB )
   if( TBB_FOUND )
      list( APPEND _cmakeArgs
         -DTBB_LIBRARIES:FILEPATH=${TBB_tbb_LIBRARY} )
   endif()
endif()

# Set up where to get VDT from.
if( ATLAS_BUILD_ROOT )
   list( APPEND _cmakeArgs
      -DVDT_LIBRARIES:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}vdt${CMAKE_SHARED_LIBRARY_SUFFIX} )
else()
   find_package( VDT )
   if( VDT_FOUND )
      list( APPEND _cmakeArgs
         -DVDT_LIBRARIES:FILEPATH=${VDT_vdt_LIBRARY} )
   endif()
endif()

# Set up where to get OpenBlas from.
find_package( BLAS )
if( BLAS_FOUND )
   list( APPEND _cmakeArgs
      -DOPENBLAS_LIBRARIES:FILEPATH=${BLAS_LIBRARIES} )
endif()

# Set up where to get XercesC from.
if( ATLAS_BUILD_XERCESC )
   list( APPEND _cmakeArgs
      -DXERCESC_INCLUDE_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_INCLUDEDIR}
      -DXERCESC_LIBRARY:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}xerces-c${CMAKE_SHARED_LIBRARY_SUFFIX}
      -DXERCESC_EXECUTABLE:FILEPATH=DummyNotNeeded )
else()
   find_package( XercesC )
   if( XERCESC_FOUND )
      list( APPEND _cmakeArgs
         -DXERCESC_INCLUDE_DIR:PATH=${XercesC_INCLUDE_DIR}
         -DXERCESC_LIBRARY:FILEPATH=${XercesC_LIBRARY}
         -DXERCESC_EXECUTABLE:FILEPATH=DummyNotNeeded )
   endif()
endif()

# Set up where to take MySQL from.
find_package( mysql )
if( MYSQL_FOUND )
   list( APPEND _cmakeArgs
      -DMYSQL_INCLUDE_DIR:PATH=${MYSQL_INCLUDE_DIR}
      -DMYSQL_LIBRARY:FILEPATH=${MYSQL_mysqlclient_LIBRARY}
      -DMYSQL_EXECUTABLE:FILEPATH=${MYSQL_mysql_EXECUTABLE} )
endif()

# Set up where to take SQLite from.
find_package( SQLite3 )
if( SQLITE3_FOUND )
   list( APPEND _cmakeArgs
      -DSQLITE_INCLUDE_DIR:PATH=${SQLite3_INCLUDE_DIR}
      -DSQLITE_LIBRARY:FILEPATH=${SQLite3_LIBRARY}
      -DSQLITE_EXECUTABLE:FILEPATH=DummyNotNeeded )
endif()

# Set up where to take EXPAT from.
find_package( EXPAT )
if( EXPAT_FOUND )
   list( APPEND _cmakeArgs
      -DEXPAT_INCLUDE_DIR:PATH=${EXPAT_INCLUDE_DIR}
      -DEXPAT_LIBRARY:FILEPATH=${EXPAT_LIBRARY}
      -DEXPAT_EXECUTABLE:FILEPATH=DummyNotNeeded )
endif()

# Set up where to take Frontier from.
find_package( Frontier_Client )
if( FRONTIER_CLIENT_FOUND )
   list( APPEND _cmakeArgs
      -DFRONTIER_CLIENT_INCLUDE_DIR:PATH=${FRONTIER_CLIENT_INCLUDE_DIR}
      -DFRONTIER_CLIENT_LIBRARY:FILEPATH=${FRONTIER_CLIENT_frontier_client_LIBRARY}
      -DFRONTIER_CLIENT_EXECUTABLE:FILEPATH=DummyNotNeeded )
endif()

# Set up where to take libaio from.
find_package( libaio )
if( LIBAIO_FOUND )
   list( APPEND _cmakeArgs
      -DLIBAIO_LIBRARIES:FILEPATH=${LIBAIO_aio_LIBRARY} )
endif()

# Set up where to take Oracle from.
find_package( Oracle )
if( ORACLE_FOUND )
   list( APPEND _cmakeArgs
      -DORACLE_INCLUDE_DIR:PATH=${ORACLE_INCLUDE_DIR}
      -DORACLE_LIBRARY:FILEPATH=${ORACLE_clntsh_LIBRARY}
      -DSQLPLUS_EXECUTABLE:FILEPATH=${SQLPLUS_EXECUTABLE} )
endif()

# Set up where to take Valgrind from.
find_package( valgrind )
if( VALGRIND_FOUND )
   list( APPEND _cmakeArgs
      -DVALGRIND_EXECUTABLE:FILEPATH=${VALGRIND_valgrind_EXECUTABLE} )
endif()

# Set up where to take libunwind from.
find_package( libunwind )
if( LIBUNWIND_FOUND )
   list( APPEND _cmakeArgs
      -DUNWIND_INCLUDE_DIR:PATH=${LIBUNWIND_INCLUDE_DIR}
      -DUNWIND_LIBRARIES:FILEPATH=${LIBUNWIND_unwind_LIBRARY} )
endif()

# Set up where to take IgProf from.
find_package( igprof )
if( IGPROF_FOUND )
   list( APPEND _cmakeArgs
      -DIGPROF_LIBRARY:FILEPATH=${IGPROF_igprof_LIBRARY}
      -DIGPROF_EXECUTABLE:FILEPATH=${IGPROF_igprof_EXECUTABLE} )
endif()

# Set up where to take GPerfTools from.
find_package( gperftools COMPONENTS tcmalloc profiler )
if( GPERFTOOLS_FOUND )
   list( APPEND _cmakeArgs
      -DGPERFTOOLS_INCLUDE_DIR:PATH=${GPERFTOOLS_INCLUDE_DIR}
      -DGPERFTOOLS_tcmalloc_LIBRARY:FILEPATH=${GPERFTOOLS_tcmalloc_LIBRARY}
      -DGPERFTOOLS_profiler_LIBRARY:FILEPATH=${GPERFTOOLS_profiler_LIBRARY}
      -DPPROF_EXECUTABLE:FILEPATH=${GPERFTOOLS_pprof_EXECUTABLE} )
endif()

# Set up where to take Qt5 from.
find_package( Qt5 COMPONENTS Core Widgets )
if( Qt5_FOUND )
   list( APPEND _cmakePrefixes "${Qt5_DIR}" "${Qt5Widgets_DIR}" )
endif()

# Set up where to take libxkbcommon from.
find_package( libxkbcommon )
if( LIBXKBCOMMON_FOUND )
   list( APPEND _cmakeArgs
      -DXCB_LIBRARY:FILEPATH=${LIBXKBCOMMON_xkbcommon_LIBRARY} )
endif()

# Extra options for the configuration.
if( NOT "${CMAKE_CXX_STANDARD}" EQUAL "" )
   list( APPEND _cmakeArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _cmakeArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( _cmakePrefixes )
   list( REMOVE_DUPLICATES _cmakePrefixes )
   list( APPEND _cmakeArgs
      -DCMAKE_PREFIX_PATH:PATH=${_cmakePrefixes} )
endif()

# Directory for the temporary build results.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/COOLBuild" )
# Directory holding the "stamp" files.
set( _stampDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/COOLStamp" )

# Build COOL for the build area.
ExternalProject_Add( COOL
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   STAMP_DIR "${_stampDir}"
   ${ATLAS_COOL_SOURCE}
   ${ATLAS_COOL_PATCH}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DBINARY_TAG:STRING=${ATLAS_PLATFORM}
   ${_cmakeArgs}
   CMAKE_COMMAND "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh"
   ${CMAKE_COMMAND}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( COOL forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_COOL_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( COOL purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for COOL"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( COOL forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the (re-)configuration of COOL"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( COOL buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove -f "${_buildDir}/cc-run"
      "${_buildDir}/cc-sh" "${_buildDir}/qmtestRun.sh"
      "${_buildDir}/run_nightly_tests_cmake.sh"
   COMMAND ${CMAKE_COMMAND} -E remove_directory -f "${_buildDir}/CoolTest"
      "${_buildDir}/env" "${_buildDir}/examples" "${_buildDir}/tests"
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing COOL into the build area"
   DEPENDEES install )
add_dependencies( Package_COOL COOL )
if( ATLAS_BUILD_CORAL )
   add_dependencies( COOL CORAL )
endif()
if( ATLAS_BUILD_PYTHON )
   add_dependencies( COOL Python )
endif()
if( ATLAS_BUILD_BOOST )
   add_dependencies( COOL Boost )
endif()
if( ATLAS_BUILD_XERCESC )
   add_dependencies( COOL XercesC )
endif()
if( ATLAS_BUILD_ROOT )
   add_dependencies( COOL ROOT )
endif()
if( ATLAS_BUILD_TBB )
   add_dependencies( COOL TBB )
endif()

# Install COOL.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
