/**
 * @file CheckerGccPlugins/src/thread_plugin.cxx
 * @author scott snyder <snyder@bnl.gov>
 * @date Sep, 2015
 * @brief Check for possible thread-safety violations.
 *
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
 */

// FIXME: can i unify pointer_is_const_arg / expr_from_arg_p / expr_from_static_p / value_from_struct?
// FIXME: are the recursion conditions in the above sufficient?

#include <unordered_set>
#include <string>
#include "checker_gccplugins.h"
#include "tree.h"
#include "function.h"
#include "basic-block.h"
#include "coretypes.h"
#include "is-a.h"
#include "predict.h"
#include "internal-fn.h"
#include "tree-ssa-alias.h"
#include "gimple-expr.h"
#include "gimple.h"
#include "gimple-iterator.h"
#include "tree-ssa-loop.h"
#include "cp/cp-tree.h"
#include "diagnostic.h"
#include "context.h"
#include "tree-pass.h"
#include "gimple-pretty-print.h"
#include "print-tree.h"
#include "tree-cfg.h"
#include "cfgloop.h"
#include "tree-ssa-operands.h"
#include "tree-phinodes.h"
#include "gimple-ssa.h"
#include "ssa-iterators.h"
#include <vector>
#include <unordered_map>
#include "stringpool.h"
#include "attribs.h"
#include "feshim.h"


using namespace CheckerGccPlugins;


namespace {


//*******************************************************************************

// stdlib functions assumed to be safe unless listed here.
// This list collected from those tagged as MT-Unsafe on the linux man pages.
const std::unordered_set<std::string> unsafe_stdlib_fns {
  "bindresvport", // glibc < 2.17
  "crypt",
  "cuserid",
  "drand48",
  "ecvt",
  "encrypt",
  "endfsent",
  "endgrent",
  "endpwent",
  "endttyent",
  "endusershell",
  "erand48",
  "ether_aton",
  "ether_ntoa",
  "exit",
  "fcloseall",
  "fcvt",
  "fmtmsg", // for glibc < 2.16
  "gamma",
  "gammaf",
  "gammal",
  "getdate",
  "getfsent",
  "getfsfile",
  "getfsspec",
  "getgrent",
  "getgrgid",
  "getgrname",
  "getlogin",
  "getlogin_r",
  "getopt",
  "getopt_long",
  "getopt_long_only",
  "getpass",
  "getpwent",
  "getpwnam",
  "getpwuid",
  "getttyent",
  "getttynam",
  "getusershell",
  "hcreate",
  "hdestroy",
  "hsearch",
  "jrand48",
  "l64a",
  "lcong48",
  "localeconv",
  "lrand48",
  "mblen",
  "mbrlen",
  "mbrtowc",
  "mbtowc",
  "mrand48",
  "mtrace",
  "muntrace",
  "nrand48",
  "ptsname",
  "putenv",
  "qecvt",
  "qfcvt",
  "re_comp",
  "re_exec",
  "readdir",
  "rexec",
  "rexec_af",
  "seed48",
  "setenv",
  "setfsent",
  "setgrent",
  "setkey",
  "setpwent",
  "setttyent",
  "setusershell",
  "siginterrupt",
  "srand48",
  "strerror",
  "strtok",
  "tmpnam",
  "ttyname",
  "ttyslot",
  "unsetenv",
  "wctomb",

  "asctime",
  "ctime",
  "gmtime",
  "localtime",
};


const char* url = "<https://gitlab.cern.ch/atlas/atlasexternals/tree/master/External/CheckerGccPlugins#thread_plugin>";


const pass_data thread_pass_data =
{
  GIMPLE_PASS, /* type */
  "thread", /* name */
  OPTGROUP_NONE,  /* optinfo_flags */
  TV_NONE, /* tv_id */
  0, /* properties_required */
  0, /* properties_provided */
  0, /* properties_destroyed */
  0, /* todo_flags_start */
  0  /* todo_flags_finish */
};


class thread_pass : public gimple_opt_pass
{
public:
  thread_pass (gcc::context* ctxt)
    : gimple_opt_pass (thread_pass_data, ctxt)
  { 
  }

  virtual unsigned int execute (function* fun) override
  { return thread_execute(fun); }

  unsigned int thread_execute (function* fun);

  virtual opt_pass* clone() override { return new thread_pass(*this); }
};


//*******************************************************************************
// General utilities.
//


/**
 * @brief Helper to format the name of the type @c t.
 */
const char* type_name (tree t)
{
  unsigned int save = flag_sanitize;
  flag_sanitize = 0;
  const char* ret = CheckerGccPlugins::type_as_string (t,
                                                       TFF_PLAIN_IDENTIFIER +
                                                       //TFF_UNQUALIFIED_NAME +
                                                       TFF_NO_OMIT_DEFAULT_TEMPLATE_ARGUMENTS);
  flag_sanitize = save;
  return ret;
}


//*******************************************************************************
// Attribute handling.
//


enum Attribute {
  ATTR_CHECK_THREAD_SAFETY,
  ATTR_THREAD_SAFE,
  ATTR_NOT_THREAD_SAFE,
  ATTR_NOT_REENTRANT,
  ATTR_NOT_CONST_THREAD_SAFE,
  ATTR_ARGUMENT_NOT_CONST_THREAD_SAFE,
  NUM_ATTR
};


const char* attr_names[NUM_ATTR] = {
  "check_thread_safety",
  "thread_safe",
  "not_thread_safe",
  "not_reentrant",
  "not_const_thread_safe",
  "argument_not_const_thread_safe",
};


typedef uint32_t Attributes_t;


Attributes_t get_attributes (tree t)
{
  Attributes_t mask = 0;

  tree attrlist = 0;
  if (DECL_P (t))
  {
    attrlist = DECL_ATTRIBUTES (t);
  }
  else if (TYPE_P (t))
  {
    attrlist = TYPE_ATTRIBUTES (t);
  }

  if (attrlist)
  {
    for (unsigned i=0; i < NUM_ATTR; i++) {
      if (lookup_attribute (attr_names[i], attrlist)) {
        mask |= (1 << i);
      }
    }
  }
  return mask;
}


bool has_attrib (Attributes_t attribs, Attribute attr)
{
  return (attribs & (1<<attr)) != 0;
}


typedef std::unordered_map<tree, std::pair<Attributes_t, location_t> > saved_attribs_t;
saved_attribs_t saved_attribs;
void check_attrib_consistency (Attributes_t attribs, tree decl)
{
  saved_attribs_t::iterator it = saved_attribs.find (decl);
  if (it == saved_attribs.end())
    saved_attribs[decl] = std::make_pair (attribs, DECL_SOURCE_LOCATION (decl));
  else if (attribs != it->second.first) {
    if (warning_at (DECL_SOURCE_LOCATION (decl), 0,
                    "Inconsistent attributes between declaration and definition of function %<%D%>.",
                    decl))
    {
      inform (it->second.second, "Declaration is here:");
      for (unsigned i=0; i < NUM_ATTR; i++) {
        Attribute ia = static_cast<Attribute> (i);
        if (has_attrib (attribs, ia) && !has_attrib (it->second.first, ia))
          inform (DECL_SOURCE_LOCATION (decl),
                  "Definition has %<%s%> but declaration does not.",
                  attr_names[i]);
        else if (!has_attrib (attribs, ia) && has_attrib (it->second.first, ia))
          inform (it->second.second,
                  "Declaration has %<%s%> but definition does not.",
                  attr_names[i]);
      }
      CheckerGccPlugins::inform_url (DECL_SOURCE_LOCATION(decl), url);
    }
  }
}


bool has_thread_safe_attrib (tree decl)
{
  return lookup_attribute ("thread_safe", DECL_ATTRIBUTES (decl));
}


//*******************************************************************************
// Tests to see if entities have various properties.
//

// Is DECL_OR_TYPE in the std:: namespace?
bool is_in_std (tree decl_or_type)
{
  tree ctx = DECL_P (decl_or_type) ? DECL_CONTEXT (decl_or_type) : TYPE_CONTEXT (decl_or_type);
  for (; ctx;
       ctx = DECL_P (ctx) ? DECL_CONTEXT (ctx) : TYPE_CONTEXT (ctx))
  {
    if (TREE_CODE (ctx) != NAMESPACE_DECL) continue;
    tree nsname = DECL_NAME (ctx);
    if (!nsname) continue;
    if (strcmp (IDENTIFIER_POINTER (nsname), "std") == 0) return true;
  }
  return false;
}


// Is TYPE a mutex type?
bool is_mutex (tree type)
{
  const char* name = CheckerGccPlugins::type_as_string (type,
                                                        TFF_PLAIN_IDENTIFIER +
                                                        TFF_SCOPE +
                                                        TFF_CHASE_TYPEDEF);
  if (strcmp (name, "std::mutex") == 0) return true;
  if (strcmp (name, "std::shared_mutex") == 0) return true;
  if (strcmp (name, "std::shared_timed_mutex") == 0) return true;
  if (strcmp (name, "std::recursive_mutex") == 0) return true;
  if (strcmp (name, "std::once_flag") == 0) return true;
  if (strcmp (name, "boost::shared_mutex") == 0) return true;
  if (strcmp (name, "TVirtualMutex") == 0) return true;
  if (strcmp (name, "ROOT::TVirtualRWMutex*") == 0) return true;
  return false;
}


// Is TYPE a mutex or an array of such?
bool is_mutex_maybearr (tree type)
{
  while (TREE_CODE (type) == ARRAY_TYPE) {
    type = TREE_TYPE (type);
  }
  return is_mutex (type);
}


// Is TYPE a std::atomic of a fundamental type?
bool is_atomic (tree type)
{
  if (TREE_CODE (type) != RECORD_TYPE) return false;
  const char* name = CheckerGccPlugins::type_as_string (type,
                                                        TFF_PLAIN_IDENTIFIER +
                                                        TFF_SCOPE +
                                                        TFF_CHASE_TYPEDEF);
  if (strncmp (name, "const ", 6) == 0)
    name += 6;

  if (startswith (name, "Gaudi::Accumulators::Counter<")) {
    // gcc 9+
    if (strstr (name, "Gaudi::Accumulators::atomicity::none>") != nullptr) {
      return false;
    }
    // gcc8 and earlier
    if (strstr (name, "(Gaudi::Accumulators::atomicity)0>") != nullptr) {
      return false;
    }
    return true;
  }

  if (startswith (name, "std::atomic<") ||
      startswith (name, "std::__atomic_base<") ||
      startswith (name, "std::__atomic_float<") )
  {
    tree targs = CLASSTYPE_TI_ARGS (type);
    if (!targs) return false;
    if (NUM_TMPL_ARGS (targs) < 1) return false;
    tree arg = TMPL_ARG (targs, 1, 0);

    if (INTEGRAL_TYPE_P (arg)) return true;
    if (SCALAR_FLOAT_TYPE_P (arg)) return true;
    if (POINTER_TYPE_P (arg)) return true;
    return false;
  }

  return false;
}


// Is TYPE a std::atomic of a fundamental type, or an array of such?
bool is_atomic_maybearr (tree type)
{
  while (TREE_CODE (type) == ARRAY_TYPE) {
    type = TREE_TYPE (type);
  }
  return is_atomic (type);
}


// True if the first argument of FNDECL is a TBuffer pointer or reference..
bool has_tbuffer_arg (tree fndecl)
{
  //tree args = FUNCTION_FIRST_USER_PARMTYPE (fndecl);
  tree args = CheckerGccPlugins::skip_artificial_parms_for
    (fndecl, TYPE_ARG_TYPES (TREE_TYPE (fndecl)));
  if (!args) return false;
  tree arg = TREE_VALUE (args);
  if (!arg) return false;
  if (!POINTER_TYPE_P (arg)) return false;
  tree ptype = TREE_TYPE (arg);
  if (TREE_CODE (ptype) != RECORD_TYPE) return false;
  tree id = TYPE_NAME (ptype);
  if (DECL_P (id)) {
    id = DECL_NAME (id);
  }
  if (TREE_CODE (id) != IDENTIFIER_NODE) return false;
  return strcmp (IDENTIFIER_POINTER (id), "TBuffer") == 0;
}


// Is TYPE a thread-local type?
bool is_thread_local (tree type)
{
  const char* name = CheckerGccPlugins::type_as_string (type,
                                                        TFF_PLAIN_IDENTIFIER +
                                                        TFF_SCOPE +
                                                        TFF_CHASE_TYPEDEF);
  if (strncmp (name, "boost::thread_specific_ptr<", 27) == 0) return true;
  return false;
}


// Does EXPR represent a static object that's not marked as thread-safe?
bool static_p (tree expr)
{
  if (!expr) return false;
  if (!TREE_STATIC (expr) && !(DECL_P (expr) && DECL_EXTERNAL (expr))) {
    return false;
  }

#if GCC_VERSION >= 14000
  if (TREE_CODE (expr) == CONSTRUCTOR) return false;
#endif

  if (DECL_P (expr)) {
    if (TREE_CODE (expr) == FUNCTION_DECL) return false;
    if (DECL_THREAD_LOCAL_P (expr)) return false;
    if (has_thread_safe_attrib (expr)) return false;
    if (is_in_std (expr)) return false;
    location_t loc = DECL_SOURCE_LOCATION (expr);
    if (loc != UNKNOWN_LOCATION) {
      const char* file = LOCATION_FILE (loc);
      if (startswith (file, "/usr/include/")) return false;
      if (strstr (file, "/xercesc/") != nullptr) return false;
    }

    tree id = DECL_NAME (expr);
    const char* name = IDENTIFIER_POINTER (id);
    if (strcmp (name, "gInterpreterMutex") == 0) return false;
    if (strcmp (name, "gSystem") == 0) return false;
  }

  return true;
}


bool const_p (tree expr)
{
#if GCC_VERSION < 14000
  // Maybe this was never needed?
  // But with gcc14, this can be set for non-const static variables
  // with no state.
  if (TREE_CONSTANT (expr)) return true;
#endif
  if (TREE_READONLY (expr)) return true;
  tree typ = TREE_TYPE (expr);
  if (typ && TREE_CODE (typ) == REFERENCE_TYPE) {
    typ = TREE_TYPE (typ);
  }
  if (typ && TREE_READONLY (typ)) return true;

  while (typ && TREE_CODE (typ) == ARRAY_TYPE) {
    typ = TREE_TYPE (typ);
    if (typ && TREE_READONLY (typ)) return true;
  }

  return false;
}


// Test to see if STMT is a call to a virtual destructor.
// This is a bit annoying, since we can't directly get the function decl
// for a virtual call.  Instead, from the call we get the class type
// and the vtable index.  We need to search the list of methods for
// the class to find the one with the proper index.
bool is_virtual_dtor_call (gimplePtr stmt)
{
  tree fndecl = vcall_fndecl (stmt);
  if (fndecl) {
    return DECL_DESTRUCTOR_P (fndecl);
  }
  return false;
}


tree service_test (tree binfo, void* bname_ptr)
{
  tree basetype = BINFO_TYPE (binfo);
  if (basetype) {
    std::string* bname = reinterpret_cast<std::string*> (bname_ptr);
    *bname = type_name (basetype);
    if (*bname == "IService") return binfo;
    if (*bname == "IInterface") return binfo;
  }
  return nullptr;
}
// Test if TYP derives from the Gaudi IService.
bool is_service (tree typ)
{
  while (POINTER_TYPE_P (typ)) {
    typ = TREE_TYPE (typ);
  }
  if (TREE_CODE (typ) != RECORD_TYPE) return false;

  std::string bname;
  tree binfo = TYPE_BINFO (typ);
  if (!binfo) return false;
  if (CheckerGccPlugins::dfs_walk_all (binfo, service_test, nullptr, &bname) != nullptr) {
    if (bname == "IInterface") {
      std::string typ_name = type_name (typ);
      return endswith (typ_name.c_str(), "Svc");
    }
    else {
      return true;
    }
  }
  return false;
}


bool is_const_method_decl (tree decl)
{
  if (TREE_CODE (decl) != FUNCTION_DECL) return false;
  tree args = DECL_ARGUMENTS (decl);
  if (!args) return false;
  tree atyp = TREE_TYPE (args);
  if (TREE_CODE (atyp) != POINTER_TYPE) return false;
  return TREE_READONLY (TREE_TYPE (atyp));
}


// Returns pair INSTDLIB, THREAD_SAFE
// If FNDECL appears to be a libc function, INSTDLIB is true.
// In that case, THREAD_SAFE is true unless the function is known
// to be unsafe.
std::pair<bool, bool> is_cstdlib (tree fndecl)
{
  location_t loc = DECL_SOURCE_LOCATION (fndecl);
  if (loc == UNKNOWN_LOCATION) return std::make_pair (false, false);
  const char* file = LOCATION_FILE (loc);
  if (startswith (file, "/usr/include/") ||
      strstr (file, "/include-fixed/") != nullptr)
  {
    // Assume functions from the system library are ok unless we know
    // they aren't.
    std::string fnname = fndecl_string (fndecl);

    if (unsafe_stdlib_fns.count (fnname) == 0)
      return std::make_pair (true, true);
    return std::make_pair (true, false);
  }
  return std::make_pair (false, false);
}


//*******************************************************************************
// Tree helpers.
//

// If EXPR is a COMPONENT_REF, array reference, etc, return the complete
// object being referenced.
tree get_inner (tree expr)
{
  if (expr && handled_component_p (expr)) {
    poly_int64 pbitsize, pbitpos;
    tree poffset;
    machine_mode pmode;
    int punsignedp, pvolatilep;
    int preversep;
    expr = get_inner_reference (expr,
                                &pbitsize,
                                &pbitpos, &poffset,
                                &pmode, &punsignedp,
                                &preversep,
                                &pvolatilep
                                );
  }
  return expr;
}



// Test to see if the type of X is a struct/class with at least one
// not_const_thread_safe member.  Returns that member or NULL_TREE.
tree has_not_const_thread_safe_member (tree x)
{
  tree typ = TREE_TYPE (x);
  if (TREE_CODE (typ) != RECORD_TYPE) return NULL_TREE;
  if (CP_AGGREGATE_TYPE_P (typ)) return NULL_TREE;
  typ = TYPE_MAIN_VARIANT (typ);

  for (tree meth = TYPE_FIELDS (typ); meth; meth = TREE_CHAIN (meth)) {
    if (TREE_CODE (meth) != FUNCTION_DECL) continue;
    if (DECL_ARTIFICIAL (meth)) continue;
    if (has_attrib (get_attributes (meth), ATTR_NOT_CONST_THREAD_SAFE)) {
      return meth;
    }
  }
  return NULL_TREE;
}


// LHS is the def of an assignment.  Check if it has been declared
// as thread_safe.
bool is_lhs_marked_thread_safe (tree lhs)
{
  if (!lhs) return false;
  if (TREE_CODE (lhs) == VAR_DECL) {
    return has_thread_safe_attrib (lhs);
  }

  if (TREE_CODE (lhs) != SSA_NAME) return false;

  tree def = lhs;

  if (!SSA_NAME_VAR (def)) {
    // For something like
    //  int* store [[gnu::thread_safe]] = const_cast<int*> (foo());
    // we can get
    //
    //  _1 = foo();
    //  store_2 = _1;
    //
    // so that the attribute isn't visible in the first stmt.
    // Check for a single immediate use.
    gimple* stmt;
    use_operand_p use_p;
    if (single_imm_use (lhs, &use_p, &stmt)) {
      if (def_operand_p def_p = SINGLE_SSA_DEF_OPERAND(stmt, SSA_OP_ALL_DEFS)) {
        def = get_def_from_ptr (def_p);
      }
    }
  }

  if (SSA_NAME_VAR (def)) {
    return has_thread_safe_attrib (SSA_NAME_VAR (def));
  }

  return false;
}


// Similar to is_lhs_marked_thread_safe, except that we take a gimple call expr.
// If there's a LHS defined in the call, use that.  Otherwise, check for
// a new expression.
//   Foo* f = new Foo;
// is translated like
//   _1 = operator new (...);
//   _2 = _1;
//   Foo::Foo(_2)
//   f = _2;
// So when we see the ctor call, we need to follow the uses of the argument
// to find the actual LHS
//
// For gcc < 12, the stmt for the ctor call may also not have the source
// location filled in.  So if we find a LHS as described above,
// also return the location attached to that stmt.
bool is_call_lhs_marked_thread_safe (gimplePtr stmt,
                                     location_t& loc)
{
  loc = gimple_location (stmt);
  if (gimple_code (stmt) != GIMPLE_CALL) return false;

  // Handle the simple case where we have a defined LHS for the call.
  tree lhs = gimple_call_lhs (stmt);
  if (lhs) return is_lhs_marked_thread_safe (lhs);

  // Test to see if this is a constructor call.
  tree fndecl = gimple_call_fndecl (stmt);
  if (!fndecl || !DECL_CONSTRUCTOR_P (fndecl)) return false;

  // Get the first argument.
  unsigned nargs = gimple_call_num_args (stmt);
  if (nargs < 1) return false;
  tree arg = gimple_call_arg (stmt, 0);
  if (TREE_CODE (arg) != SSA_NAME) return false;

  // Loop over uses.  Ignore the ctor call itself.
  // When we find an assignment, do the LHS test.
  // Fail if we find anything else.
  use_operand_p use_p;
  imm_use_iterator iter;
  bool saw_assign = false;
  bool thread_safe = false;
  FOR_EACH_IMM_USE_FAST (use_p, iter, arg) {
    gimplePtr use_stmt = USE_STMT (use_p);
    if (use_stmt == stmt) continue;
    if (is_gimple_assign (use_stmt) && !saw_assign) {
      saw_assign = true;
      thread_safe = is_lhs_marked_thread_safe (gimple_op (use_stmt, 0));
      loc = gimple_location (use_stmt);
    }
    else {
      return false;
    }
  }

  return thread_safe;
}

// Test to see if a pointer value comes directly or indirectly from
// a const pointer function argument.
tree pointer_is_const_arg (tree val)
{
  //fprintf (stderr, "pointer_is_const_arg_p\n");
  //debug_tree(val);
  tree valtest = get_inner (val);
  tree valtype = TREE_TYPE (valtest);
  if (!POINTER_TYPE_P(valtype) || !TYPE_READONLY (TREE_TYPE (valtype)))
    return NULL_TREE;

  if (TREE_CODE (val) == ADDR_EXPR)
    val = TREE_OPERAND (val, 0);
  if (TREE_CODE (val) == COMPONENT_REF)
    val = get_inner (val);
  if (TREE_CODE (val) == MEM_REF)
    val = TREE_OPERAND (val, 0);

  if (TREE_CODE (val) != SSA_NAME) return NULL_TREE;
  if (SSA_NAME_VAR (val) && TREE_CODE (SSA_NAME_VAR (val)) == PARM_DECL) return val;

  gimplePtr stmt = SSA_NAME_DEF_STMT (val);
  if (!stmt) return NULL_TREE;
  //debug_gimple_stmt (stmt);
  //fprintf (stderr, "code %s\n", get_tree_code_name(gimple_expr_code(stmt)));
  
  if (is_gimple_assign (stmt) && (gimple_expr_code(stmt) == SSA_NAME ||
                                  gimple_expr_code(stmt) == VAR_DECL ||
                                  gimple_expr_code(stmt) == PARM_DECL ||
                                  gimple_expr_code(stmt) == POINTER_PLUS_EXPR ||
                                  gimple_expr_code(stmt) == ADDR_EXPR))
  {
    //fprintf (stderr, "recurse\n");
    return pointer_is_const_arg (gimple_op(stmt, 1));
  }
  else if (gimple_code (stmt) == GIMPLE_PHI) {
    size_t nop = gimple_num_ops (stmt);
    for (size_t i = 0; i < nop; i++) {
      tree op = gimple_op (stmt, i);
      tree ret = pointer_is_const_arg (op);
      if (ret) return ret;
    }
  }
  return NULL_TREE;
}


// Test to see if a value comes directly or indirectly from
// a structure.  If so returns a tuple S, FIELD, THREAD_SAFE.
// S is the structure object if so, FIELD is the referenced member,
// and THREAD_SAFE is true if the value is tagged as thread_safe.
// Otherwise S is returned as null.
// If REQUIRE_POINTER is true, then require that the value be a pointer.
std::tuple<tree, tree, bool>
value_from_struct (tree val, bool require_pointer)
{
  if (TREE_CODE (val) == COMPONENT_REF) {
    return std::make_tuple (get_inner (val), TREE_OPERAND (val, 1), false);
  }

  std::tuple<tree, tree, bool> null (NULL_TREE, NULL_TREE, false);

  tree valtest = get_inner (val);
  tree valtype = TREE_TYPE (valtest);
  while (TREE_CODE (val) != SSA_NAME) {
    switch (TREE_CODE (val)) {
    case COMPONENT_REF:
      {
        tree inner = get_inner (val);
        tree field = TREE_OPERAND (val, 1);
        if (!DECL_FIELD_IS_BASE (field)) {
          return std::make_tuple (inner, field, false);
        }
        val = inner;
        break;
      }
    case ADDR_EXPR:
    case ARRAY_REF:
    case MEM_REF:
      val = TREE_OPERAND (val, 0);
      break;
    case SSA_NAME:
      break;
    default:
      return null;
    }
  }

  if (require_pointer && !POINTER_TYPE_P(valtype)) {
    return null;
  }

  gimplePtr stmt = SSA_NAME_DEF_STMT (val);
  if (!stmt) return null;
  //debug_gimple_stmt (stmt);
  //fprintf (stderr, "code %s\n", get_tree_code_name(gimple_expr_code(stmt)));
  bool thread_safe = is_lhs_marked_thread_safe (val);
  
  if ((is_gimple_assign (stmt) || gimple_nop_p (stmt)) &&
      (gimple_expr_code(stmt) == SSA_NAME ||
       gimple_expr_code(stmt) == VAR_DECL ||
       gimple_expr_code(stmt) == PARM_DECL ||
       gimple_expr_code(stmt) == POINTER_PLUS_EXPR ||
       gimple_expr_code(stmt) == ADDR_EXPR ||
       gimple_expr_code(stmt) == ARRAY_REF ||
       gimple_expr_code(stmt) == COMPONENT_REF))
  {
    //fprintf (stderr, "recurse\n");
    auto ret = value_from_struct (gimple_op(stmt, 1), require_pointer);
    std::get<2> (ret) |= thread_safe;
    return ret;
  }
  else if (gimple_code (stmt) == GIMPLE_PHI) {
    size_t nop = gimple_num_ops (stmt);
    for (size_t i = 0; i < nop; i++) {
      tree op = gimple_op (stmt, i);
      auto ret = value_from_struct (op, require_pointer);
      if (std::get<0>(ret)) {
        std::get<2> (ret) |= thread_safe;
        return ret;
      }
    }
  }
  return std::make_tuple (NULL_TREE, NULL_TREE, thread_safe);
}


// Test to see if a pointer value comes directly or indirectly from a pointer
// member in a structure, also recognizing std::unique_ptr and
// std::shared_ptr.
//  If so returns a tuple S, FIELD, THREAD_SAFE.
// S is the structure object if so, FIELD is the referenced member,
// and THREAD_SAFE is true if the value is tagged as thread_safe.
// Otherwise S is returned as null.
std::tuple<tree, tree, bool>
pointer_from_struct (tree val)
{
  std::tuple<tree, tree, bool> null (NULL_TREE, NULL_TREE, false);

  bool thread_safe = false;
  {
    auto ret = value_from_struct (val, false);
    thread_safe = std::get<2> (ret);
    if (std::get<0> (ret)) {
      tree typ = TREE_TYPE (std::get<1>(ret));
      while (TREE_CODE (typ) == ARRAY_TYPE) {
        typ = TREE_TYPE (typ);
      }
      if (!POINTER_TYPE_P (typ)) return null;
      return ret;
    }
  }

  if (TREE_CODE (val) != SSA_NAME) return null;
  gimplePtr stmt = SSA_NAME_DEF_STMT (val);
  while (is_gimple_assign (stmt)) {
    tree val1 = gimple_op (stmt, 1);
    if (TREE_CODE (val1) != SSA_NAME) return null;
    stmt = SSA_NAME_DEF_STMT (val1);
  }
  if (!is_gimple_call (stmt)) return null;

  // Test for calling std::unique_ptr<>::operator->
  tree fndecl = gimple_call_fndecl (stmt);
  if (!fndecl) return null;
  tree name = DECL_NAME (fndecl);
  if (!name) return null;
  const char* namestr = IDENTIFIER_POINTER (name);

  if (strcmp (namestr, "operator->") != 0 &&
      strcmp (namestr, "operator*") != 0 &&
      strcmp (namestr, "get") != 0 &&
      strcmp (namestr, "release") != 0)
  {
    return null;
  }

  tree cls = DECL_CONTEXT (fndecl);
  if (!cls) return null;
  if (TREE_CODE (cls) != RECORD_TYPE) return null;
  if (!is_in_std (cls)) return null;
  tree clsname = TYPE_IDENTIFIER (cls);
  if (!clsname) return null;
  const char* clsnamestr = IDENTIFIER_POINTER (clsname);
  if (strcmp (clsnamestr, "unique_ptr") != 0) return null;

  // Test that the unique_ptr is a member.
  if (gimple_call_num_args (stmt) < 1) return null;
  tree arg = gimple_call_arg (stmt, 0);

  auto ret = value_from_struct (arg, false);
  std::get<2>(ret) |= thread_safe;
  return ret;
}


// Test if VAL is a pointer member of this that is not marked thread_safe.
// If so, return the field tree; otherwise return null.
tree pointer_from_this_struct (tree val)
{
  auto [s, field, thread_safe]= pointer_from_struct (val);
  if (!s || thread_safe) return NULL_TREE;
  if (has_thread_safe_attrib (field)) return NULL_TREE;
  if (TREE_CODE (s) == MEM_REF)
    s = TREE_OPERAND (s, 0);
  if (TREE_CODE (s) != SSA_NAME) return NULL_TREE;
  s = SSA_NAME_VAR (s);
  if (!s) return NULL_TREE;
  if (TREE_CODE (s) != PARM_DECL) return NULL_TREE;
  if (DECL_NAME (s) != this_identifier) return NULL_TREE;
  return field;
}


// Check to see if VAL derives from a function argument.
bool expr_from_arg_p (tree val)
{
  //debug_tree(val);
  if (TREE_CODE (val) == ADDR_EXPR)
    val = TREE_OPERAND (val, 0);
  if (TREE_CODE (val) == COMPONENT_REF)
    val = get_inner (val);
  if (TREE_CODE (val) == MEM_REF)
    val = TREE_OPERAND (val, 0);

  if (TREE_CODE (val) != SSA_NAME) return false;
  if (SSA_NAME_VAR (val) && TREE_CODE (SSA_NAME_VAR (val)) == PARM_DECL) return true;

  gimplePtr stmt = SSA_NAME_DEF_STMT (val);
  if (!stmt) return false;
  //debug_gimple_stmt (stmt);
  //fprintf (stderr, "code %s\n", get_tree_code_name(gimple_expr_code(stmt)));

  if (is_gimple_assign (stmt) && (gimple_expr_code(stmt) == VAR_DECL ||
                                  gimple_expr_code(stmt) == PARM_DECL ||
                                  gimple_expr_code(stmt) == POINTER_PLUS_EXPR ||
                                  gimple_expr_code(stmt) == ADDR_EXPR ||
                                  gimple_expr_code(stmt) == MEM_REF))
  {
    //fprintf (stderr, "recurse\n");
    return expr_from_arg_p (gimple_op(stmt, 1));
  }
  else if (gimple_code (stmt) == GIMPLE_PHI) {
    size_t nop = gimple_num_ops (stmt);
    for (size_t i = 0; i < nop; i++) {
      tree op = gimple_op (stmt, i);
      bool ret = expr_from_arg_p (op);
      if (ret) return ret;
    }
  }
  return false;
}


// Check to see if VAL derives from a static object.
tree expr_from_static_p (tree val)
{
  //debug_tree(val);
  if (static_p (val)) return val;
  
  if (TREE_CODE (val) == ADDR_EXPR)
    val = TREE_OPERAND (val, 0);
  if (TREE_CODE (val) == COMPONENT_REF)
    val = get_inner (val);
  if (TREE_CODE (val) == MEM_REF)
    val = TREE_OPERAND (val, 0);

  if (TREE_CODE (val) == VAR_DECL && static_p (val)) return val;
  if (TREE_CODE (val) != SSA_NAME) return NULL_TREE;

  gimplePtr stmt = SSA_NAME_DEF_STMT (val);
  if (!stmt) return NULL_TREE;
  //debug_gimple_stmt (stmt);
  //fprintf (stderr, "code %s\n", get_tree_code_name(gimple_expr_code(stmt)));

  if (is_gimple_assign (stmt) && (gimple_expr_code(stmt) == VAR_DECL ||
                                  gimple_expr_code(stmt) == PARM_DECL ||
                                  gimple_expr_code(stmt) == POINTER_PLUS_EXPR ||
                                  gimple_expr_code(stmt) == ADDR_EXPR ||
                                  gimple_expr_code(stmt) == MEM_REF))
  {
    //fprintf (stderr, "recurse\n");
    return expr_from_static_p (gimple_op(stmt, 1));
  }
  else if (gimple_code (stmt) == GIMPLE_PHI) {
    size_t nop = gimple_num_ops (stmt);
    for (size_t i = 0; i < nop; i++) {
      tree op = gimple_op (stmt, i);
      tree ret = expr_from_static_p (op);
      if (ret) return ret;
    }
  }
  return NULL_TREE;
}


//*******************************************************************************


void check_mutable (tree expr, gimplePtr stmt, function* fun, const char* what)
{
  while (true) {
    switch (TREE_CODE (expr)) {
    case COMPONENT_REF:
      if (TYPE_READONLY(TREE_TYPE(TREE_OPERAND(expr, 0))) &&
          DECL_MUTABLE_P (TREE_OPERAND (expr, 1)))
      {
        tree op = TREE_OPERAND (expr, 1);
        tree optype = TREE_TYPE (op);
        if (has_thread_safe_attrib (op)) return;
        if (is_mutex_maybearr (optype)) return;
        if (is_atomic_maybearr (optype)) return;
        if (is_thread_local (optype)) return;
        if (warning_at (gimple_location (stmt), 0,
                        "%s %<%E%> of type %<%T%> within thread-safe function %<%D%>; may not be thread-safe",
                        what, op, optype, fun->decl))
        {
          CheckerGccPlugins::inform_url (gimple_location (stmt), url);
        }
        return;
      }
      expr = TREE_OPERAND (expr, 0);
      break;

    case ARRAY_REF:
    case ARRAY_RANGE_REF:
    case BIT_FIELD_REF:
    case REALPART_EXPR:
    case IMAGPART_EXPR:
    case VIEW_CONVERT_EXPR:
      expr = TREE_OPERAND (expr, 0);
      break;

    default:
      return;
    }
  }
}


// Check for direct use of a static value.
void check_direct_static_use (Attributes_t attribs, gimplePtr stmt, function* fun)
{
  if (has_attrib (attribs, ATTR_NOT_REENTRANT))
    return;

  size_t nop = gimple_num_ops (stmt);
  for (size_t i = 0; i < nop; i++) {
    tree op = gimple_op (stmt, i);

    tree optest = get_inner (op);
    if (static_p (optest)) {
      if (!const_p (optest)) {

        // Allow suppressing the warning for an individual expression.
        bool suppressed = false;
        if (is_gimple_assign (stmt) && gimple_expr_code (stmt) == VAR_DECL) {
          tree lhs = gimple_get_lhs (stmt);
          if (lhs && is_lhs_marked_thread_safe (lhs)) {
            suppressed = true;
          }
        }

        if (!suppressed && !is_mutex(TREE_TYPE(op))) {
          if (warning_at (gimple_location (stmt), 0,
                          "Use of static expression %<%E%> of type %<%T%> within function %<%D%> may not be thread-safe.",
                          op, TREE_TYPE(op),fun->decl))
          {
            if (DECL_P (optest)) {
              inform (DECL_SOURCE_LOCATION (optest),
                      "Declared here:");
            }
            CheckerGccPlugins::inform_url (gimple_location (stmt), url);
          }
        }
      }
      else if (tree meth = has_not_const_thread_safe_member (optest)) {
        if (warning_at (gimple_location (stmt), 0,
                        "Use of %<const%> %<static%> expression %<%E%> of type %<%T%> within function %<%D%> may not be thread-safe.",
                        op, TREE_TYPE(op), fun->decl))
        {
          if (DECL_P (optest)) {
            inform (DECL_SOURCE_LOCATION (optest),
                    "Declared here:");
          }
          inform (DECL_SOURCE_LOCATION (meth),
                  "Because it has a method declared %<not_const_thread_safe%>:");
          CheckerGccPlugins::inform_url (gimple_location (stmt), url);
        }
      }
    }
  }
}


// Check for assigning from an address of a static object
// into a pointer/reference, or for discarding const.
//   OP: operand to check
//   STMT: gimple statement being checked
//   FUN: function being checked
void check_assign_address_of_static (Attributes_t attribs,
                                     tree op,
                                     gimplePtr stmt,
                                     function* fun)
{
  if (op && TREE_CODE (op) == ADDR_EXPR) {
    while (op && TREE_CODE (op) == ADDR_EXPR)
      op = TREE_OPERAND (op, 0);
  }

  tree optest = get_inner (op);
  if (static_p (optest) && !has_attrib (attribs, ATTR_NOT_REENTRANT)) {
    if (!const_p (optest)) {

      // Allow suppressing the warning for an individual expression.
      bool suppressed = false;
      if (is_gimple_assign (stmt) && gimple_expr_code (stmt) == VAR_DECL) {
        tree lhs = gimple_get_lhs (stmt);
        if (lhs && is_lhs_marked_thread_safe (lhs)) {
          suppressed = true;
        }
      }

      if (!suppressed && !is_atomic(TREE_TYPE(op)) && !is_mutex(TREE_TYPE(op))) {
        if (warning_at (gimple_location (stmt), 0,
                        "Pointer or reference bound to %<static%> expression %<%E%> of type %<%T%> within function %<%D%>; may not be thread-safe.",
                        op, TREE_TYPE(op), fun->decl))
        {
          if (DECL_P (optest)) {
            inform (DECL_SOURCE_LOCATION (optest),
                    "Declared here:");
          }
          CheckerGccPlugins::inform_url (gimple_location (stmt), url);
        }
      }
    }
    else if (tree meth = has_not_const_thread_safe_member (optest)) {
      if (warning_at (gimple_location (stmt), 0,
                      "Use of %<const%> %<static%> expression %<%E%> of type %<%T%> within function %<%D%> may not be thread-safe.",
                      op, TREE_TYPE(op), fun->decl))
      {
        if (DECL_P (optest)) {
          inform (DECL_SOURCE_LOCATION (optest),
                  "Declared here:");
        }
        inform (DECL_SOURCE_LOCATION (meth),
                "Because it has a method declared %<not_const_thread_safe%>:");
        CheckerGccPlugins::inform_url (gimple_location (stmt), url);
      }
    }
  }
}


void warn_about_discarded_const (Attributes_t attribs,
                                 tree expr,
                                 gimplePtr stmt,
                                 function* fun)
{
  tree parm = pointer_is_const_arg (expr);
  if (parm) {
    if (has_attrib (attribs, ATTR_ARGUMENT_NOT_CONST_THREAD_SAFE)) return;
    if (expr == parm) {
      if (warning_at (gimple_location (stmt), 0,
                      "%<const%> discarded from expression %<%E%> of type %<%T%> within function %<%D%>; may not be thread-safe",
                      expr, TREE_TYPE(expr), fun->decl))
      {
        CheckerGccPlugins::inform_url (gimple_location (stmt), url);
      }
    }
    else {
      if (warning_at (gimple_location (stmt), 0,
                      "%<const%> discarded from expression %<%E%> of type %<%T%> (deriving from parameter %<%E%>) within function %<%D%>; may not be thread-safe",
                      expr, TREE_TYPE(expr), parm, fun->decl))
      {
        CheckerGccPlugins::inform_url (gimple_location (stmt), url);
      }
    }
  }
  else {
    if (has_attrib (attribs, ATTR_NOT_CONST_THREAD_SAFE)) return;
    if (warning_at (gimple_location (stmt), 0,
                    "%<const%> discarded from expression %<%E%> of type %<%T%> within function %<%D%>; may not be thread-safe",
                    expr, TREE_TYPE(expr), fun->decl))
    {
      CheckerGccPlugins::inform_url (gimple_location (stmt), url);
    }
  }
}


// Called when LHS does not have const type.
void check_discarded_const (Attributes_t attribs,
                            tree op,
                            tree lhs,
                            gimplePtr stmt,
                            function* fun)
{
  tree optest = get_inner (op);
  tree optype = TREE_TYPE (optest);
  if (POINTER_TYPE_P(optype) && TYPE_READONLY (TREE_TYPE (optype))) {
    if (!is_lhs_marked_thread_safe (lhs)) {
      // Allow const_cast if LHS is explicitly marked thread_safe.
      warn_about_discarded_const (attribs, op, stmt, fun);
    }
  }
}


void check_pointer_write_from_const_member (gimplePtr stmt,
                                            function* fun)
{
  if (!DECL_CONST_MEMFUNC_P (fun->decl)) return;
  if (!is_gimple_assign (stmt)) return;
  tree lhs = gimple_op (stmt, 0);
  if (!lhs) return;

  if (TREE_CODE (lhs) == MEM_REF) {
    tree p = TREE_OPERAND (lhs, 0);
    tree p_type = TREE_TYPE (p);
    if (POINTER_TYPE_P (p_type) && TREE_CODE(p) == SSA_NAME) {
      tree field = pointer_from_this_struct (p);
      if (field) {
        if (warning_at (gimple_location (stmt), 0,
                        "Pointer write from member %<%E%> of type %<%T%> in const member function; may not be thread-safe",
                        field, TREE_TYPE(field)))
        {
          CheckerGccPlugins::inform_url (gimple_location (stmt), url);
        }
      }
    }
  }
}


void check_assignments (Attributes_t attribs, gimplePtr stmt, function* fun)
{
  if (gimple_code (stmt) != GIMPLE_ASSIGN) return;
  size_t nop = gimple_num_ops (stmt);
  if (nop < 2) return;

  tree lhs = gimple_op (stmt, 0);
  if (!lhs) return;

  // Inside a dtor, the compiler can be making assignments to internal `_vptr*'
  // fields even though `this' is const.
  if (TREE_CODE (lhs) == COMPONENT_REF) {
    tree field = TREE_OPERAND (lhs, 1);
    if (field && TREE_CODE (field) == FIELD_DECL) {
      tree ident = DECL_NAME (field);
      if (ident && startswith (IDENTIFIER_POINTER (ident), "_vptr")) return;
    }
  }

  check_mutable (lhs, stmt, fun, "Setting mutable field");
  check_pointer_write_from_const_member (stmt, fun);

  // Is the LHS a pointer/ref?
  tree lhs_type = TREE_TYPE (lhs);
  if (!POINTER_TYPE_P (lhs_type)) return;
  bool lhs_const = TYPE_READONLY (TREE_TYPE (lhs_type));

  // Ignore internal assignments of vtbl pointers.
  // We see these in calls via member function pointers.
  if (TREE_TYPE (lhs_type) == vtbl_ptr_type_node) {
    return;
  }

  // Does RHS point to something static, or is it const or mutable?
  for (size_t i = 1; i < nop; i++) {
    tree op = gimple_op (stmt, i);

    // Check for discarding const if LHS is non-const.
    if (!lhs_const) {
      check_discarded_const (attribs, op, lhs, stmt, fun);
    }
    
    check_assign_address_of_static (attribs, op, stmt, fun);

    if (op && TREE_CODE (op) == ADDR_EXPR) {
      while (op && TREE_CODE (op) == ADDR_EXPR)
        op = TREE_OPERAND (op, 0);
    }

    if (!lhs_const)
      check_mutable (op, stmt, fun, "Taking non-const reference to mutable field");
  }
}


// Warn about checked function calling unchecked function,
// unless known to be ok.
void check_thread_safe_call (Attributes_t attribs,
                             tree fndecl,
                             gimplePtr stmt,
                             function* fun)
{
  if (check_thread_safety_p (fndecl)) return;

  // Don't warn if this call is explicitly marked thread-safe.
  location_t sloc;
  if (is_call_lhs_marked_thread_safe (stmt, sloc)) return;

  bool unsafe_stdlib = false;
  location_t loc = DECL_SOURCE_LOCATION (fndecl);
  if (loc != UNKNOWN_LOCATION) {
    if (is_in_std (fndecl)) return;

    {
      auto p = is_cstdlib (fndecl);
      if (p.first) {
        if (p.second) return;
        unsafe_stdlib = true;
      }
    }
    const char* file = LOCATION_FILE (loc);
    if (endswith (file, "/mm_malloc.h"))
      return;
    if (endswith (file, "/emmintrin.h"))
      return;
    if (endswith (file, "/unwind.h"))
      return;

    // Handle list of strings from the config file.
    for (const std::string& s : CheckerGccPlugins::config["thread.thread_safe_calls"]) {
      if (strstr (file, s.c_str()) != nullptr)
        return;
    }
  }

  std::string fnname = fndecl_string (fndecl);

  if (startswith (fnname.c_str(), "__")) return;
  if (startswith (fnname.c_str(), "boost::")) return;
  if (startswith (fnname.c_str(), "Eigen::")) return;
  if (fnname == "operator new") return;
  if (fnname == "operator new []") return;
  if (fnname == "operator delete") return;
  if (fnname == "operator delete []") return;
  if (fnname == "_mm_pause") return;

  // ROOT functions.
  if (fnname == "operator<<" || fnname == "operator>>") {
    if (has_tbuffer_arg (fndecl)) return;
  }
  if (fnname == "ROOT::GenerateInitInstanceLocal") return;

  if (unsafe_stdlib || has_attrib (attribs, ATTR_NOT_THREAD_SAFE)) {
    if (warning_at (sloc, 0,
                    "Non-thread-safe function %<%D%> called from thread-safe function %<%D%>; may not be thread-safe.",
                    fndecl, fun->decl))
    {
      inform (loc, "Declared here:");
      CheckerGccPlugins::inform_url (sloc, url);
    }

    return;
  }

  tree ctx = DECL_CONTEXT (fndecl);
  if (RECORD_OR_UNION_TYPE_P (ctx))
  {
    Attributes_t clattribs = get_attributes (ctx);
    if (has_attrib (clattribs, ATTR_NOT_THREAD_SAFE)) {
      if (warning_at (sloc, 0,
                      "Function %<%D%> in non-thread-safe class %<%T%> called from thread-safe function %<%D%>; may not be thread-safe.",
                      fndecl, ctx, fun->decl))
      {
        inform (loc, "Declared here:");
        CheckerGccPlugins::inform_url (sloc, url);
      }

      return;
    }
  }

  static const bool unchecked_calls_warning = !CheckerGccPlugins::config["thread.unchecked_calls_warning"].empty();
  if (unchecked_calls_warning) {
    if (warning_at (sloc, 0,
                    "Unchecked function %<%D%> called from thread-safe function %<%D%>; may not be thread-safe.",
                    fndecl, fun->decl))
    {
      inform (loc, "Declared here:");
      CheckerGccPlugins::inform_url (sloc, url);
    }
  }
}


// A function calling a not_reentrant function must be not_reentrant.
void check_not_reentrant_call (Attributes_t attribs,
                               Attributes_t fnattribs,
                               tree fndecl,
                               gimplePtr stmt,
                               function* fun)
{
  if (has_attrib (fnattribs, ATTR_NOT_REENTRANT) &&
      !has_attrib (attribs, ATTR_NOT_REENTRANT))
  {
    if (warning_at (gimple_location (stmt), 0,
                    "Function %<%D%> calling %<not_reentrant%> function %<%D%> must also be %<not_reentrant%>.",
                    fun->decl, fndecl))
    {
      CheckerGccPlugins::inform_url (gimple_location (stmt), url);
    }
  }
}


// A const member function calling a not_const_thread_safe on the same
// object must also be not_const_thread_safe.
void check_not_const_thread_safe_call (Attributes_t attribs,
                                       Attributes_t fnattribs,
                                       tree fndecl,
                                       gimplePtr stmt,
                                       function* fun)
{
  if (!DECL_CONST_MEMFUNC_P (fun->decl)) return;
  if (!has_attrib (fnattribs, ATTR_NOT_CONST_THREAD_SAFE)) return;
  if (has_attrib (attribs, ATTR_NOT_CONST_THREAD_SAFE)) return;
  if (gimple_call_num_args (stmt) < 1) return;
  tree arg0 = gimple_call_arg (stmt, 0);
  if (TREE_CODE (arg0) != SSA_NAME) return;
  tree var = SSA_NAME_VAR (arg0);
  if (!var) return;
  if (TREE_CODE (var) != PARM_DECL) return;
  if (DECL_NAME (var) != this_identifier) return;

  if (warning_at (gimple_location (stmt), 0,
                  "%<const%> member function %<%D%> calling %<not_const_thread_safe%> member function %<%D%> with same object must also be %<not_const_thread_safe%>",
                  fun->decl, fndecl))
  {
    CheckerGccPlugins::inform_url (gimple_location (stmt), url);
  }
}


// A function passing an argument on to an argument_not_const_thread_safe
// function must also be argument_not_const_thread_safe.
void check_argument_not_const_thread_safe_call (Attributes_t attribs,
                                                Attributes_t fnattribs,
                                                tree fndecl,
                                                gimplePtr stmt,
                                                function* fun)
{
  if (!has_attrib (fnattribs, ATTR_ARGUMENT_NOT_CONST_THREAD_SAFE)) return;
  if (has_attrib (attribs, ATTR_ARGUMENT_NOT_CONST_THREAD_SAFE)) return;

  // Check to see if any of the arguments being passed to fndecl
  // come from an argument.
  unsigned nargs = gimple_call_num_args (stmt);
  for (unsigned i=0; i < nargs; i++) {
    tree arg = gimple_call_arg (stmt, i);
    if (expr_from_arg_p (arg)) {
      if (warning_at (gimple_location (stmt), 0,
                      "Function %<%D%> passing argument %<%E%> of type %<%T%> to %<argument_not_const_thread_safe%> function %<%D%> must also be %<argument_not_const_thread_safe%>.",
                      fun->decl, arg, TREE_TYPE(arg), fndecl))
      {
        CheckerGccPlugins::inform_url (gimple_location (stmt), url);
      }
    }
  }

  if (!DECL_CONST_MEMFUNC_P (fun->decl)) return;

  // Check to see if any of the arguments being passed to fndecl
  // come from member data.
  for (unsigned i=0; i < nargs; i++) {
    tree arg = gimple_call_arg (stmt, i);
    auto [s, field, tsdum] = value_from_struct (arg, false);
    
    if (s) {
      if (TREE_CODE (s) == MEM_REF)
        s = TREE_OPERAND (s, 0);
      if (TREE_CODE (s) != SSA_NAME) continue;
      s = SSA_NAME_VAR (s);
      if (TREE_CODE (s) != PARM_DECL) continue;
      if (DECL_NAME (s) != this_identifier) continue;
      //debug_tree (s);
      if (warning_at (gimple_location (stmt), 0,
                      "%<const%> member function %<%D%> passing member data %<%E%> of type %<%T%> to %<argument_not_const_thread_safe%> function %<%D%> must also be %<argument_not_const_thread_safe%>",
                      fun->decl, field, TREE_TYPE(field), fndecl))
      {
        CheckerGccPlugins::inform_url (gimple_location (stmt), url);
      }
    }
  }
}


void check_static_argument_not_const_thread_safe_call (Attributes_t attribs,
                                                       Attributes_t fnattribs,
                                                       tree fndecl,
                                                       gimplePtr stmt,
                                                       function* fun)
{
  if (!has_attrib (fnattribs, ATTR_ARGUMENT_NOT_CONST_THREAD_SAFE)) return;
  if (has_attrib (attribs, ATTR_NOT_REENTRANT)) return;

  // Check to see if any of the arguments being passed to fndecl
  // come from const static data.
  unsigned nargs = gimple_call_num_args (stmt);
  for (unsigned i=0; i < nargs; i++) {
    tree arg = gimple_call_arg (stmt, i);
    if (expr_from_static_p (arg)) {
      if (warning_at (gimple_location (stmt), 0,
                      "Function %<%D%> passing %<const%> %<static%> argument %<%E%> of type %<%T%> to %<argument_not_const_thread_safe%> function %<%D%> must be %<not_reentrant%>.",
                      fun->decl, arg, TREE_TYPE(arg), fndecl))
      {
        CheckerGccPlugins::inform_url (gimple_location (stmt), url);
      }
    }
  }
}


// Check for a nonconst call through a pointer/reference member
// in a const member function.
void check_nonconst_call_from_const_member (tree fndecl,
                                            gimplePtr stmt,
                                            function* fun)
{
  if (!DECL_CONST_MEMFUNC_P (fun->decl)) return;
#if GCC_VERSION >= 14000
  if (!DECL_OBJECT_MEMBER_FUNCTION_P (fndecl) || DECL_CONST_MEMFUNC_P (fndecl)) return;
#else
  if (!DECL_NONSTATIC_MEMBER_FUNCTION_P (fndecl) || DECL_CONST_MEMFUNC_P (fndecl)) return;
#endif
  if (gimple_call_num_args (stmt) < 1) return;
  tree arg0 = gimple_call_arg (stmt, 0);
  if (TREE_CODE (arg0) != SSA_NAME) return;
  tree field = pointer_from_this_struct (arg0);
  if (field) {
    if (is_service (TREE_TYPE (field))) return;
    if (warning_at (gimple_location (stmt), 0,
                    "Function %<%D%> calling non-const function %<%D%> via member pointer/reference %<%E%> of type %<%T%>; may not be thread-safe",
                    fun->decl, fndecl, field, TREE_TYPE (field)))
    {
      CheckerGccPlugins::inform_url (gimple_location (stmt), url);
    }
  }
}


// Check passing an address of a static object to a called function
// by non-const pointer/ref.
//   ARG_TYPE: type of argument
//   ARG: argument to test
//   STMT: gimple statement being checked
//   FUN: function being checked
void check_pass_static_by_call (Attributes_t attribs,
                                tree fndecl,
                                tree arg_type,
                                tree arg,
                                gimplePtr stmt,
                                function* fun)
{
  if (!POINTER_TYPE_P (arg_type)) return;

  if (arg && TREE_CODE (arg) == ADDR_EXPR) {
    while (arg && TREE_CODE (arg) == ADDR_EXPR)
      arg = TREE_OPERAND (arg, 0);
  }
  tree argtest = get_inner (arg);

  if (static_p (argtest) && !has_attrib (attribs, ATTR_NOT_REENTRANT)) {
    if (!const_p (argtest)) {

      tree arg_test = arg_type;
      if (POINTER_TYPE_P (arg_test))
        arg_test = TREE_TYPE (arg_test);

      // Ok if it's an atomic value or a mutex..
      if (is_atomic (arg_test)) return;
      if (is_mutex (arg_test)) return;

      // FNDECL could be null in the case of a call through a function pointer.
      // Try to print a nice error in that case.
      tree fnerr = fndecl;
      if (!fnerr) {
        fnerr = error_mark_node;
        tree fn = gimple_call_fn (stmt);
        if (TREE_CODE (fn) == SSA_NAME &&
            FUNCTION_POINTER_TYPE_P (TREE_TYPE (fn)))
        {
          gimplePtr def_stmt = SSA_NAME_DEF_STMT (fn);
          if (is_gimple_assign (def_stmt)) {
            fnerr = gimple_op (def_stmt, 1);
          }
        }
      }

      if (warning_at (gimple_location (stmt), 0,
                      "%<static%> expression %<%E%> of type %<%T%> passed to pointer or reference function argument of %<%E%> within function %<%D%>; may not be thread-safe",
                      arg, TREE_TYPE(arg), fnerr, fun->decl))
      {
        if (DECL_P (argtest)) {
          inform (DECL_SOURCE_LOCATION (argtest),
                  "Declared here:");
        }
        CheckerGccPlugins::inform_url (gimple_location (stmt), url);
      }
    }
  }
}


// Check for discarding const from a pointer/ref in a function call.
//   ARG_TYPE: type of argument
//   ARG: argument to test
//   STMT: gimple statement being checked
//   FUN: function being checked
void check_discarded_const_in_funcall (Attributes_t attribs,
                                       tree arg_type,
                                       tree arg,
                                       gimplePtr stmt,
                                       function* fun)
{
  bool lhs_const = TYPE_READONLY (TREE_TYPE (arg_type));

  // Non-const reference binding to something that's not const is ok.
  if (!lhs_const && TREE_CODE (arg_type) == REFERENCE_TYPE &&
      !TYPE_READONLY (TREE_TYPE (arg)))
  {
    return;
  }

  // We can get a false positive if a function return value is declared const;
  // for example:
  //    const std::string foo()
  //    {
  //      std::string tmp;
  //      tmp += 'a';
  //      return tmp;
  //    }
  //
  // In the call to operator+= above, the this argument will be the
  // SSA_NAME referencing the RESULT_DECL for the function return value,
  // which is marked READONLY.  Avoid this by skipping the test for
  // the outermost pointer types for a RESULT_DECL.
  bool is_result = (TREE_CODE (arg) == SSA_NAME &&
                    SSA_NAME_VAR (arg) &&
                    TREE_CODE (SSA_NAME_VAR (arg)) == RESULT_DECL);

  tree argtest = get_inner (arg);

  tree ctest = TREE_TYPE (argtest);
  while (POINTER_TYPE_P (ctest) && POINTER_TYPE_P (arg_type)) {
    if (!is_result &&
        TREE_READONLY (TREE_TYPE (ctest)) &&
        !TREE_READONLY (TREE_TYPE (arg_type)))
    {
      // We don't want to warn about calls to destructors.
      // We've already filtered out calls to non-virtual destructors,
      // but virtual destructors are more work to recognize.
      // So don't make that test until we're ready to emit a warning.
      if (!is_virtual_dtor_call (stmt))
      {
        warn_about_discarded_const (attribs, arg, stmt, fun);
      }
      return;
    }
    ctest = TREE_TYPE (ctest);
    arg_type = TREE_TYPE (arg_type);
    is_result = false;
  }
}


// Check for discarding const from the return value of a function.
//   STMT: gimple statement being checked
//   FUN: function being checked
void check_discarded_const_from_return (Attributes_t attribs,
                                        gimplePtr stmt,
                                        function* fun)
{
  tree lhs = gimple_call_lhs (stmt);
  if (!lhs) return;
  tree lhs_type = TREE_TYPE (lhs);
  if (!POINTER_TYPE_P (lhs_type)) return;
  bool lhs_const = TYPE_READONLY (TREE_TYPE (lhs_type));
  if (lhs_const) return;

  if (is_lhs_marked_thread_safe (lhs)) return;

  const gcall *gc = GIMPLE_CHECK2<const gcall *> (stmt);
  tree rhs_type = gimple_call_return_type (gc);
  if (POINTER_TYPE_P (rhs_type))
    rhs_type = TREE_TYPE (rhs_type);

  if (rhs_type && TYPE_READONLY (rhs_type))
  {
    warn_about_discarded_const (attribs, gimple_call_fn(stmt), stmt, fun);
  }
}


// Check for a pointer member being passed to a nonconst pointer argument
// from a const member function.
void check_nonconst_pointer_member_passing (tree fndecl,
                                            int i, tree arg_type, tree arg,
                                            gimplePtr stmt,
                                            function* fun)
{
  // In case of a const reference bound to a pointer, consider instead
  // the underlying pointer type.
  if (TREE_CODE (arg_type) == REFERENCE_TYPE &&
      TREE_READONLY (TREE_TYPE (arg_type)) &&
      POINTER_TYPE_P (TREE_TYPE (arg_type)))
  {
    arg_type = TREE_TYPE (arg_type);
  }
  if (!POINTER_TYPE_P (arg_type)) return;

  // Only check if we're in a const member function.
  if (!DECL_CONST_MEMFUNC_P (fun->decl)) return;

  // If we're calling a non-static member function, don't check for the
  // first (this) argument.  That will be checked separately.
  if (!fndecl) return;
#if GCC_VERSION >= 14000
  if (DECL_OBJECT_MEMBER_FUNCTION_P (fndecl) && i == 0) return;
#else
  if (DECL_NONSTATIC_MEMBER_FUNCTION_P (fndecl) && i == 0) return;
#endif

  // Don't warn about thread-safe libc functions.
  if (is_cstdlib (fndecl).second) return;

  // Ok if we're passing to a const pointer.
  if (TREE_READONLY (TREE_TYPE (arg_type))) return;

  // Don't complain about calls to std::size.
  // ??? Can we generalize this?
  if (is_in_std (fndecl)) {
    tree fnname = DECL_NAME (fndecl);
    if (fnname) {
      const char* fnname_s = IDENTIFIER_POINTER (fnname);
      if (strcmp (fnname_s, "size") == 0) return;
      if (strcmp (fnname_s, "as_const") == 0) return;
    }
  }

  // Warn if arg comes from a pointer member.
  tree field = pointer_from_this_struct (arg);
  if (field) {
    if (is_service (TREE_TYPE (field))) return;
    if (warning_at (gimple_location (stmt), 0,
                    "Argument from member %<%E%> of type %<%T%> in const member function passed to non-const pointer argument of function %<%D%>; may not be thread-safe",
                    field, TREE_TYPE (field), fndecl))
    {
      inform (DECL_SOURCE_LOCATION (fndecl), "Called function declared here:");
      CheckerGccPlugins::inform_url (gimple_location (stmt), url);
    }
  }
}


void check_calls (Attributes_t attribs, gimplePtr stmt, function* fun)
{
  if (gimple_code (stmt) != GIMPLE_CALL) return;

  // Can happen with ubsan.
  if (gimple_call_fn (stmt) == NULL_TREE) return;

  check_discarded_const_from_return (attribs, stmt, fun);

  bool ctor_dtor_p = false;

  tree fndecl = gimple_call_fndecl (stmt);
  if (!fndecl) {
    fndecl = vcall_fndecl (stmt);
  }
  if (fndecl) {
    // Skip calls to compiler-internal functions.
    if (DECL_ARTIFICIAL (fndecl)) return;
    tree name = DECL_NAME (fndecl);
    if (name) {
      const char* namestr = IDENTIFIER_POINTER (name);

      // Skip names starting with __.  Exception: constructors and destructors
      // will have names like __ct_ and __dt_; don't skip those.
      if (namestr && namestr[0] == '_' && namestr[1] == '_' &&
          !startswith (namestr, "__ct_") &&
          !startswith (namestr, "__dt_"))
      {
        return;
      }
      if (strcmp (namestr, "operator delete") == 0) return;
      if (strcmp (namestr, "operator delete []") == 0) return;
    }
    
    Attributes_t fnattribs = get_attributes (fndecl);
    check_thread_safe_call (fnattribs, fndecl, stmt, fun);
    check_not_reentrant_call (attribs, fnattribs, fndecl, stmt, fun);
    check_not_const_thread_safe_call (attribs, fnattribs, fndecl, stmt, fun);
    check_argument_not_const_thread_safe_call (attribs, fnattribs, fndecl, stmt, fun);
    check_static_argument_not_const_thread_safe_call (attribs, fnattribs, fndecl, stmt, fun);
    check_nonconst_call_from_const_member (fndecl, stmt, fun);

    if (DECL_CONSTRUCTOR_P (fndecl) || DECL_DESTRUCTOR_P (fndecl))
      ctor_dtor_p = true;
  }

  unsigned nargs = gimple_call_num_args (stmt);
  if (nargs < 1) return;
  tree fntype = TREE_TYPE (gimple_call_fn (stmt));
  if (TREE_CODE (fntype) == POINTER_TYPE)
    fntype = TREE_TYPE (fntype);
  tree arg_types = TYPE_ARG_TYPES (fntype);

  for (unsigned i=0; arg_types && i < nargs; i++, arg_types = TREE_CHAIN(arg_types))
  {
    if (i == 0 && ctor_dtor_p) continue;
    
    tree arg_type = TREE_VALUE (arg_types);
    tree arg = gimple_call_arg (stmt, i);

    if (!POINTER_TYPE_P (arg_type)) continue;

    check_discarded_const_in_funcall (attribs, arg_type, arg, stmt, fun);
    check_pass_static_by_call (attribs, fndecl, arg_type, arg, stmt, fun);
    check_nonconst_pointer_member_passing (fndecl, i, arg_type, arg, stmt, fun);
  }
}


void check_returns (gimplePtr stmt, function* fun)
{
  if (gimple_code (stmt) != GIMPLE_RETURN) return;
  //debug_gimple_stmt (stmt);
  tree retval = gimple_op (stmt, 0);
  auto [s, field, tsdum] = value_from_struct (retval, true);

  // Check for use of ATLAS_THREAD_SAFE in a return; e.g.,
  //   int& c_nc [[gnu::thread_safe]] =  const_cast<int&>(c);
  //   return c_nc;
  if (is_lhs_marked_thread_safe (retval)) return;
  if (TREE_CODE (retval) == SSA_NAME) {
    gimplePtr defstmt = SSA_NAME_DEF_STMT (retval);
    if (defstmt && gimple_code (defstmt) == GIMPLE_ASSIGN) {
      tree val = gimple_op (defstmt, 1);
      if (val && is_lhs_marked_thread_safe (val)) return;
    }
  }

  // Don't warn about function pointers.
  if (field &&
      TREE_CODE (TREE_TYPE (field)) == POINTER_TYPE &&
      TREE_CODE (TREE_TYPE (TREE_TYPE (field))) == FUNCTION_TYPE)
  {
    return;
  }

  if (s && TREE_READONLY (TREE_TYPE (s))) {
    if (warning_at (gimple_location (stmt), 0,
                    "Returning non-%<const%> pointer or reference member %<%E%> of type %<%T%> from structure %<%D%> within %<const%> member function %<%D%>; may not be thread-safe.",
                    field, TREE_TYPE(field), TREE_TYPE (s), fun->decl))
    {
      inform (DECL_SOURCE_LOCATION (field), "Declared here:");
      CheckerGccPlugins::inform_url (gimple_location (stmt), url);
    }
  }
}


unsigned int thread_pass::thread_execute (function* fun)
{
  bool checked_attribs = false;
  if (DECL_ATTRIBUTES (fun->decl))
  {
    Attributes_t attribs = get_attributes (fun->decl);
    check_attrib_consistency (attribs, fun->decl);
    checked_attribs = true;
  }

  // Return if we're not supposed to check this function.
  if (!check_thread_safety_p (fun->decl))
    return 0;

  // Also skip compiler-internal functions.
  if (fun->decl) {
    tree name = DECL_NAME (fun->decl);
    if (name) {
      const char* namestr = IDENTIFIER_POINTER (name);
      if (namestr && namestr[0] == '_' && namestr[1] == '_') {
        // Don't skip constructors and destructors.
        if (!startswith (namestr, "__ct_") && !startswith (namestr, "__dt_"))
        {
          return 0;
        }
      }

      // ROOT special case.
      // The ClassDef macro injects this into user classes; the inline
      // definition of this uses static data.
      if (strcmp (namestr, "CheckTObjectHashConsistency") == 0) {
        return 0;
      }
    }
  }

  Attributes_t attribs = get_attributes (fun->decl);
  if (!checked_attribs) {
    check_attrib_consistency (attribs, fun->decl);
  }

  const bool static_memfunc_p = DECL_CONST_MEMFUNC_P (fun->decl);
  tree rettype = TREE_TYPE (DECL_RESULT (fun->decl));
  const bool nonconst_pointer_return_p = POINTER_TYPE_P (rettype) && !TYPE_READONLY (TREE_TYPE (rettype));
  const bool not_const_thread_safe = has_attrib(attribs, ATTR_NOT_CONST_THREAD_SAFE);

  basic_block bb;
  FOR_EACH_BB_FN(bb, fun) {
    for (gimple_stmt_iterator si = gsi_start_bb (bb); 
         !gsi_end_p (si);
         gsi_next (&si))
    {
      gimplePtr stmt = gsi_stmt (si);
      //debug_gimple_stmt (stmt);

      check_direct_static_use (attribs, stmt, fun);
      check_assignments (attribs, stmt, fun);
      check_calls (attribs, stmt, fun);

      if (static_memfunc_p &&
          nonconst_pointer_return_p && !not_const_thread_safe)
        check_returns (stmt, fun);
    }
  }

  return 0;
}


// Warn about static/mutable fields in class type TYPE.
void check_mutable_static_fields (tree type)
{
  tree decl = TYPE_NAME (type);

  for (tree f = TYPE_FIELDS (type); f; f = DECL_CHAIN (f)) {
    if (TREE_CODE (f) == FIELD_DECL)
    {
      if (DECL_MUTABLE_P (f) &&
          !has_thread_safe_attrib (f) &&
          !is_mutex_maybearr (TREE_TYPE (f)) &&
          !is_atomic_maybearr (TREE_TYPE (f)) &&
          !is_thread_local (TREE_TYPE (f)))
      {
        if (warning_at (DECL_SOURCE_LOCATION(f), 0,
                        "%<mutable%> member %<%D%> of type %<%T%> within thread-safe class %<%D%>; may not be thread-safe",
                        f, TREE_TYPE(f), decl))
        {
          CheckerGccPlugins::inform_url (DECL_SOURCE_LOCATION(f), url);
        }
      }
    }
    else if (TREE_CODE (f) == VAR_DECL)
    {
      if (static_p (f) &&
          !const_p (f) &&
          !has_thread_safe_attrib (f) &&
          !is_atomic (TREE_TYPE (f)) &&
          !is_mutex  (TREE_TYPE (f)))
      {
        if (warning_at (DECL_SOURCE_LOCATION(f), 0,
                        "%<static%> member %<%D%> of type %<%T%> within thread-safe class %<%D%>; may not be thread-safe",
                        f, TREE_TYPE(f), decl))
        {
          CheckerGccPlugins::inform_url (DECL_SOURCE_LOCATION(f), url);
        }
      }
    }
  }
}


void find_overridden_functions_r (tree type,
                                  tree fndecl,
                                  std::vector<tree>& basedecls);

// FNDECL is a virtual function defined in TYPE.
// Fill BASEDECLS with base class functions overridden by FNDECL.
// Based on look_for_overrides from search.c.
void find_overridden_functions (tree type,
                                tree fndecl,
                                std::vector<tree>& basedecls)
{
  tree binfo = TYPE_BINFO (type);
  tree base_binfo;

  for (int ix = 0; BINFO_BASE_ITERATE (binfo, ix, base_binfo); ix++)
    {
      tree basetype = BINFO_TYPE (base_binfo);

      if (TYPE_POLYMORPHIC_P (basetype)) {
	find_overridden_functions_r (basetype, fndecl, basedecls);
      }
    }
}


void find_overridden_functions_r (tree type,
                                  tree fndecl,
                                  std::vector<tree>& basedecls)
{
  tree fn = CheckerGccPlugins::look_for_overrides_here (type, fndecl);
  if (fn) {
    basedecls.push_back (fn);
    return;
  }

  /* We failed to find one declared in this class. Look in its bases.  */
  find_overridden_functions (type, fndecl, basedecls);
}


void check_attrib_match (tree type, tree fndecl, tree basedecl, const char* attrname)
{
  bool b_has = lookup_attribute (attrname, DECL_ATTRIBUTES (basedecl));
  bool d_has = lookup_attribute (attrname, DECL_ATTRIBUTES (fndecl));

  if (b_has && !d_has) {
    if (warning_at (DECL_SOURCE_LOCATION(fndecl), 0,
                    "%<virtual%> function %<%D%> within class %<%D%> does not have attribute %<%s%>, but overrides %<%D%> which does",
                    fndecl, type, attrname, basedecl))
    {
      inform (DECL_SOURCE_LOCATION (basedecl),
              "Overridden function declared here:");
      CheckerGccPlugins::inform_url (DECL_SOURCE_LOCATION(fndecl), url);
    }
  }
  else if (d_has && !b_has) {
    if (warning_at (DECL_SOURCE_LOCATION(fndecl), 0,
                    "%<virtual%> function %<%D%> within class %<%D%> has attribute %<%s%>, but overrides %<%D%> which does not",
                    fndecl, type, attrname, basedecl))
    {
      inform (DECL_SOURCE_LOCATION (basedecl),
              "Overridden function declared here:");
      CheckerGccPlugins::inform_url (DECL_SOURCE_LOCATION(fndecl), url);
    }
  }
}


// Warn about overriding virtual functions with inconsistent threading attributes.
void check_virtual_overrides (tree type)
{
  for (tree meth = TYPE_FIELDS (type); meth; meth = TREE_CHAIN (meth)) {
    if (TREE_CODE (meth) != FUNCTION_DECL) continue;
    if (DECL_ARTIFICIAL (meth)) continue;
    if (!DECL_VIRTUAL_P (meth)) continue;

    std::vector<tree> basedecls;
    find_overridden_functions (type, meth, basedecls);

    for (tree basedecl : basedecls) {
      std::string fnname = fndecl_string (basedecl);

      // Read from the config file the list of methods the we allow overriding
      // with inconsistent threading attributes.
      const static std::unordered_set<std::string> initMethods
        (CheckerGccPlugins::config["thread.init_methods"].begin(),
         CheckerGccPlugins::config["thread.init_methods"].end());

      if (initMethods.find (fnname) == initMethods.end() &&
          !((fnname == "Gaudi::Algorithm::execute" ||
             fnname == "Algorithm::execute") &&
            !is_const_method_decl (basedecl)))
      {
        check_attrib_match (type, meth, basedecl, "not_thread_safe");
      }

      check_attrib_match (type, meth, basedecl, "not_reentrant");
      check_attrib_match (type, meth, basedecl, "thread_safe");
      check_attrib_match (type, meth, basedecl, "argument_not_const_thread_safe");

      // Skip checking for Gaudi types that we don't want to change.
      if (fnname != "IRegistry::object" &&
          fnname != "IMessageSvc::defaultStream")
      {
        check_attrib_match (type, meth, basedecl, "not_const_thread_safe");
      }
    }
  }
}


// Called after a type declaration.
// Check class/struct members here.
void thread_finishtype_callback (void* gcc_data, void* /*user_data*/)
{
  tree type = (tree)gcc_data;
  if (TREE_CODE (type) != RECORD_TYPE) return;

  // Skip checking `aggregate' types --- essentially POD types.
  // However, this test is not reliable for template types --- we get called
  // with the template itself.  The template will not have had the
  // not_aggregate flag set, so CP_AGGREGATE_TYPE will always be true.
  if (CP_AGGREGATE_TYPE_P (type) && !CLASSTYPE_TEMPLATE_INFO (type)) return;

  tree decl = TYPE_NAME (type);
  if (!check_thread_safety_p (decl)) return;
  check_mutable_static_fields (type);
  check_virtual_overrides (type);
}


void thread_finishdecl_callback (void* gcc_data, void* /*user_data*/)
{
  tree decl = (tree)gcc_data;
  if (TREE_CODE(decl) == FUNCTION_DECL) {
    Attributes_t attribs = get_attributes (decl);
    check_attrib_consistency (attribs, decl);
  }
}


//******************************************************************************
// Dummy do-nothing pass.  Used to replace free_lang_data in LTO builds.
// See below.
//

const pass_data dummy_pass_data =
{
  SIMPLE_IPA_PASS, /* type */
  "*dummy", /* name */
  OPTGROUP_NONE,  /* optinfo_flags */
  TV_NONE, /* tv_id */
  0, /* properties_required */
  0, /* properties_provided */
  0, /* properties_destroyed */
  0, /* todo_flags_start */
  0  /* todo_flags_finish */
};


class dummy_pass : public simple_ipa_opt_pass
{
public:
  dummy_pass (gcc::context* ctxt)
    : simple_ipa_opt_pass (dummy_pass_data, ctxt)
  { 
  }

  virtual unsigned int execute (function* ) override
  { return 0; }

  virtual opt_pass* clone() override { return new dummy_pass(*this); }
};


} // anonymous namespace


void init_thread_checker (plugin_name_args* plugin_info)
{
  struct register_pass_info thread_pass_info = {
    new thread_pass(g),
    "ssa",
    0,
    PASS_POS_INSERT_AFTER
  };

  register_callback (plugin_info->base_name,
                     PLUGIN_PASS_MANAGER_SETUP,
                     NULL,
                     &thread_pass_info);

  // The free_lang_data pass runs just after gimplification
  // (and before the conversion to SSA).  Normally, it doesn't do much,
  // but if LTO is enabled, then it will free the C++-specific parts
  // from the types and declarations.  However, the thread checker
  // relies on this information.  Further, dataflow analysis requires that
  // the SSA conversion has taken place, so we can't just shift it
  // to before free_lang_data.  But it doesn't really matter that much
  // exactly where free_lang_data runs, so we instead move it to slightly
  // later, after the checker runs.
  //
  // The plugin API doesn't seem to provide a way to reorder existing passes,
  // so we do this by replacing free_lang_data with a dummy and then adding
  // a new instance of it further on.
  if (flag_lto) {
    struct register_pass_info dummy_pass_info = {
      new dummy_pass(g),
      "*free_lang_data",
      0,
      PASS_POS_REPLACE
    };

    struct register_pass_info freelang_pass_info = {
      make_pass_ipa_free_lang_data(g),
      "build_ssa_passes",
      1,
      PASS_POS_INSERT_AFTER
    };

    register_callback (plugin_info->base_name,
                       PLUGIN_PASS_MANAGER_SETUP,
                       NULL,
                       &dummy_pass_info);

    register_callback (plugin_info->base_name,
                       PLUGIN_PASS_MANAGER_SETUP,
                       NULL,
                       &freelang_pass_info);
  }

  register_callback (plugin_info->base_name,
                     PLUGIN_FINISH_TYPE,
                     thread_finishtype_callback,
                     NULL);

  register_callback (plugin_info->base_name,
                     PLUGIN_FINISH_DECL,
                     thread_finishdecl_callback,
                     NULL);
}
