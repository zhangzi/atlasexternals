// Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration.
// thread22_test: testing check_nonconst_call_from_const_member

#pragma ATLAS check_thread_safety

class IService
{
public:
  virtual ~IService();
};

class Service : public IService {};

class AthService : public Service {};

namespace std {
template <class T>
class unique_ptr
{
public:
  T* operator-> [[ATLAS::not_const_thread_safe]] () const { return m_p; }
  T* get [[ATLAS::not_const_thread_safe]] () const { return m_p; }
  T* release [[ATLAS::not_const_thread_safe]] () { return m_p; }
private:
  T* m_p;
};
}

struct Helper {
  void calc();
};

struct Helper2;

struct Helper7 {
  virtual ~Helper7();
  void calc();
};

struct Helper8 : public Helper7 { };

struct MyTool {
  void doIt() const;
  Helper* m_helper;
  Helper2* m_helper2;
  std::unique_ptr<Helper> m_helper3;
  Helper* m_helper4[2];
  std::unique_ptr<Helper> m_helper5[2];
  Helper* m_helper6 [[ATLAS::thread_safe]];
  Helper7* m_helper7;
  Helper8* m_helper8;
};

struct Helper2 : public AthService
{
  void calc();
};

void MyTool::doIt() const {
  m_helper->calc();  // should warn here
  Helper* h1 = m_helper;
  h1->calc();  // should warn here
  Helper* h2 [[ATLAS::thread_safe]] = m_helper;
  h2->calc(); // no warning here
  m_helper2->calc();  // no warning here
  m_helper3->calc();  // should warn here
  m_helper4[0]->calc();  // should warn here
  m_helper5[0]->calc();  // should warn here
  m_helper6->calc();  // no warning here
  m_helper7->calc();  // should warn here
  m_helper8->calc();  // should warn here

  m_helper3.get()->calc();  // should warn here.
  m_helper5[0].get()->calc();  // should warn here.
  Helper* h3 = m_helper3.get();
  h3->calc(); // should warn here.
  Helper* h4 = m_helper5[0].get();
  h4->calc(); // should warn here.
  Helper* h5 [[ATLAS::thread_safe]] = m_helper3.get();
  h5->calc(); // no warning here.
}
