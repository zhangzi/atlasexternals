// Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration.
// thread20_test: testing lambda handling in check_thread_safety_p.

#pragma ATLAS check_thread_safety


class KitManager {
public:
  static void instance [[ATLAS::not_thread_safe]] ();
  KitManager() [[ATLAS::not_thread_safe]];
  ~KitManager() [[ATLAS::not_thread_safe]];
};


namespace {
  bool registered = ( []() [[ATLAS::not_thread_safe]] {
                        KitManager::instance();
                        return true;
                      } ) ();
}


KitManager::KitManager()
{
  instance();
}


KitManager::~KitManager()
{
  instance();
}


